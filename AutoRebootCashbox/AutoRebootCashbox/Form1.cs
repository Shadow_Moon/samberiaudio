﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoRebootCashbox
{
    public partial class Form1 : Form
    {        
        const string first = "/c echo n | plink.exe tc@10.";
        const string second = ".35.";
        const string third = @" -pw ""324012"" /usr/local/bin/cash restart";
        public Form1()
        {
            InitializeComponent();
            dateTimePicker1.Value = DateTime.Now.AddMinutes(-2);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                dateTimePicker1.Enabled = false;
            }
            else
            {
                checkBox2.Checked = false;
                dateTimePicker1.Enabled = true;
            }
        }

        void Reboot ()
        {
            string[] numberCashMask;
            string[] numberCash = new string[256];
            numberCashMask = textBoxMaskCash.Text.Split(',');
            for (int i = 0, k = 0; i < numberCashMask.Length; i++)
            {
                if (!numberCashMask[i].Contains("-"))
                {
                    numberCash[i + k] = numberCashMask[i];
                    continue;
                }
                else
                {
                    var tmp = numberCashMask[i].Split('-');
                    var from = Convert.ToInt32(tmp[0]);
                    var to = Convert.ToInt32(tmp[1]);
                    for (; !(to - from == -1);)
                    {
                        numberCash[i + k] = from.ToString();
                        from++;
                        
                        if (to - from == -1)
                            break;
                        k++;


                    }
                }
            }
            for (int i = 0; i < numberCash.Length; i++)
            {
                if (numberCash[i] != null)
                {
                    System.Diagnostics.Process p = new System.Diagnostics.Process();
                    var startString = first + maskedTextBoxGM.Text.Trim(' ') + second + numberCash[i] + third;
                    p.StartInfo.FileName = "cmd.exe";
                    p.StartInfo.Arguments = startString;
                    p.StartInfo.UseShellExecute = false;
                    p.Start();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Reboot();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false && checkBox2.Checked == true)
            {
                if (dateTimePicker1.Value.Year == DateTime.Now.Year)
                    if (dateTimePicker1.Value.Month == DateTime.Now.Month)
                        if (dateTimePicker1.Value.Day == DateTime.Now.Day)
                            if (dateTimePicker1.Value.Hour == DateTime.Now.Hour)
                                if (dateTimePicker1.Value.Minute == DateTime.Now.Minute)
                                {
                                    Reboot();
                                    checkBox2.Checked = false;
                                    System.Threading.Thread.Sleep(90 * 1000);
                                }
            }
        }
    }
}
