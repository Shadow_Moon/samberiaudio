﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Runtime.InteropServices;
using System.Diagnostics.Eventing.Reader;
using Microsoft.Exchange.WebServices.Data;

namespace AutoOvervalue
{
    public partial class Form1 : Form
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private const int DELAY = 1000;
        private System.Diagnostics.Process C1;
        private const string CONFPATH = "./config.ini";
        private Config config;

        public Form1()
        {
            InitializeComponent();
            config = new Config();
#if DEBUG
            button1.Enabled = true;
            checkBox1.Enabled = true;
#endif
            LoadConfigFromFile();            
        }


        private void SaveConfigToFile(object sender, EventArgs e)
        {
            try
            {
                config.TimeMorning = Convert.ToDateTime(maskedTextBox1.Text);
                config.TimeEvening = Convert.ToDateTime(maskedTextBox2.Text);
                
                if (textBox2.Text == "")
                    config.Gm = 0;
                else
                    config.Gm = Convert.ToInt32(textBox2.Text);                
            }
            catch (Exception)
            {


                //MessageBox.Show("Проверить данные в времени печати");
            }
            try
            {
                config.Multiply = Convert.ToDouble(textBox1.Text);
            }
            catch (Exception)
            {

                MessageBox.Show("(1/действий в минуту) имеет неправильный формат - надо через запятую и никаких букв");
            }
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Config));
            using (var fStream = new FileStream(CONFPATH, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlSerializer.Serialize(fStream, config);
            }
        }

        private void LoadConfigFromFile()
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Config));
            try
            {
                using (var fStream = new FileStream(CONFPATH, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    try
                    {
                        config = (Config)xmlSerializer.Deserialize(fStream);
                    }
                    catch (InvalidOperationException)
                    {
                        logger.Info("Файл config.ini неисправен");
                        return;
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                logger.Info("Файла config.ini не найдено");
                return;
            }
            config.Multiply = Convert.ToDouble(textBox1.Text);
            textBox2.Text = config.Gm.ToString();
            maskedTextBox1.Text = LoadDateTime(config.TimeMorning);
            maskedTextBox2.Text = LoadDateTime(config.TimeEvening);
        }

        private string LoadDateTime(DateTime dt)
        {
            string str;
            if (dt.Hour <= 9)
                str = "0" + dt.Hour.ToString();
            else
                str = dt.Hour.ToString();
            if (dt.Minute <= 9)
                str += ":" + "0" + dt.Minute.ToString();
            else
                str += ":" + dt.Minute.ToString();
            return str;
        }

        private string DateTimeForClicker(DateTime dt)
        {
            string str;
            if (dt.Day <= 9)
                str = "0" + dt.Day.ToString();
            else
                str = dt.Day.ToString();
            if (dt.Month <= 9)
                str += "0" + dt.Month.ToString();
            else
                str += dt.Month.ToString();
            str += dt.Year.ToString();
            return str;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SendEmail(true);
            //Clicker(false);
        //    var str = DateTimeForClicker(DateTime.Now) + "06";
        }

        private void Sleep(double multiply)
        {
            Thread.Sleep((int)(DELAY * multiply));
        }

        private void Loop(int count, string key, double multiply)
        {
            for (int i = 0; i < count; i++)
            {
                SendKeys.SendWait(key);
                logger.Trace("Отправлено нажатие {0}, пауза на {1} секунд", key, multiply*config.Multiply);
                Sleep(multiply * config.Multiply);
            }
        }

        // Activate an application window.
        [DllImport("USER32.DLL")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        private void Clicker(bool evening)
        {
            ClickerOpen1C();
            ClickerSendToPrinter(evening);
            SendEmail(evening);
        }

        private void LoopPerChar(int count, string key, double multiply)
        {
            for (int i = 0; i < count; i++)
            {
                for (int j = 0; j < key.Length; j++)
                {
                    SendKeys.SendWait(key[j].ToString());
                    logger.Trace("Отправлено нажатие {0}, пауза на {1} секунд", key[j], multiply * config.Multiply);
                    Sleep(multiply * config.Multiply);
                } 
            }
        }

        private void ClickerSendToPrinter(bool evening)
        {
            if (evening == true)
            {
                var str = "";
                if (checkBox1.Checked == true)
                    str = DateTimeForClicker(DateTime.Now) + "04";
                else
                    str = DateTimeForClicker(DateTime.Now) + "06";
                //LoopPerChar(1, str, 1);
                Loop(2, "{TAB}", 1);
                logger.Trace("Нажали два раза ТАБ");

                LoopPerChar(1, str, 1);
                logger.Trace("Изменили время начала переоценок на {0}", str);
                Loop(3, "{TAB}", 1);
                logger.Trace("Нажали три раза ТАБ");
            }
            else
            {
                Loop(5, "{TAB}", 1);
                logger.Trace("Нажали Tab 5 раз");
            }
            Loop(1, "{ENTER}", 0.5);
            logger.Trace("Нажали Ентер по кнопке Заполнить");
            Loop(2, "{DOWN}", 0.5);
            logger.Trace("Нажали два раза вниз для выбора Оперативные");
#if DEBUG
            Loop(1, "{ENTER}", 45); // debug
#else
            Loop(1, "{ENTER}", 450);
#endif            
            logger.Trace("Нажали Ентер, выбрав Оперативные, и ждем 7.5 минут пока сгенерит");
            Loop(7, "{TAB}", 0.5);
            logger.Trace("Нажимаем 7 раз таб до чекбокса Исключить промо");
            Loop(1, " ", 0.5);
            logger.Trace("Убираем галочку Исключить промо");
            Loop(1, "{TAB}", 0.5);
            logger.Trace("Нажимаем 1 раз таб до Только с остатками");
            Loop(1, " ", 0.5);
            logger.Trace("Ставим галочку Только с остатками");
            Loop(5, "{TAB}", 0.5);
            logger.Trace("Нажимаем 5 раз таб, переходим в выбор ценников");
            Loop(1, "{ENTER}", 0.5);
            logger.Trace("Нажимаем ентер, входим в список шаблонов ценников");
            Loop(1, "{DOWN}", 0.5);
            logger.Trace("Выбираем Автоматические ценники. ВАЖНО, УСТАНОВИТЕ ПРАВИЛЬНЫЙ ЦЕННИК В КОНСТАНТАХ");
#if DEBUG
            logger.Trace("Нажимаем ентер, 15 минут формируются ценники");
            Loop(1, "{ENTER}", 90); // debug
#else
            Loop(1, "{ENTER}", 900);
#endif

#if DEBUG
#else
            logger.Trace("Нажимаем контрл+Р, ждем 60 секунд");
            Loop(1, "^p", 60);
            Loop(1, "{ENTER}", 120);
            logger.Trace("Нажат ентер, запуск печати");
#endif
            Loop(1, "%{F4}", 10);
            logger.Trace("Нажат alt+F4");
            Loop(1, "{RIGHT}", 10);
            Loop(1, "{ENTER}", 10);
            logger.Trace("Переоценка напечатана");
        }

        private void ClickerOpen1C()
        {
            string processString = string.Format(@"ENTERPRISE /S ""s{0}-bd\sm-complex s{0} sql""", config.Gm);
            try
            {
                //C1 = System.Diagnostics.Process.Start(@"C:\Program Files (x86)\1cv82\8.2.19.130\bin\1cv8.exe", processString);
                C1 = System.Diagnostics.Process.Start(@"C:\Program Files (x86)\1cv82\common\1cestart.exe", processString);
                logger.Trace("****************Запустилась 1С****************");
            }
            catch (Exception)
            {
                logger.Trace("1C не запустилась, чтото не так");
                return;
            }
            SetForegroundWindow(C1.Handle);
            Sleep(30);
            logger.Trace("Ждем 30 секунд после запуска 1С");
            Loop(1, " ", 2);
            logger.Trace("Нажали пробел, подтверждение ком тайны");
            
            Loop(2, "{ENTER}", 2);
            logger.Trace("Нажали ентер закрывая окно");
            //return;
            Sleep(10);
            Loop(1, "%", 1);
            logger.Trace("Нажимали ALT");
            Sleep(1);
            Loop(3, "{LEFT}", 0.5);
            logger.Trace("Нажимали три раза влево до Сервис");
            Loop(17, "{DOWN}", 0.5);
            logger.Trace("Нажали 17 раз вниз, до Работа с торг оборудованием");
            Loop(1, "{RIGHT}", 0.5);
            logger.Trace("Нажали 1 раз вправо");
            Loop(4, "{DOWN}", 0.5);
            logger.Trace("Нажали 4 раза вниз");
            Loop(1, "{ENTER}", 0.5);
            logger.Trace("Нажали Enter для открытия обработки и ждем 10 секунд её открытия");
            Sleep(10);
        }

        private void SendEmail (bool isEvening)
        {

//#if DEBUG
          //  return;
//#endif
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2013_SP1);

            service.AutodiscoverUrl("sdivprint@samberi.com");

            service.Credentials = new WebCredentials("sdivprint", "ra9bit");

            EmailMessage message = new EmailMessage(service);
            message.Body = "Доброе утро!" + Environment.NewLine;
            message.Subject = string.Format("Печать переоценки Самбери-{0}", config.Gm);

            string logType = "Microsoft-Windows-PrintService/Operational";
            //string query = "*[System/EventID=307]";
            //string query = "*";
            string query = string.Format("*[System[TimeCreated[@SystemTime>'{0}']]]", DateTime.Now.AddHours(-1).ToUniversalTime().ToString("s"));
            var elQuery = new EventLogQuery(logType, PathType.LogName, query);
            var elReader = new EventLogReader(elQuery);

            for (EventRecord eventInstance = elReader.ReadEvent(); eventInstance != null; eventInstance = elReader.ReadEvent())
            {
                if (eventInstance.Id == 307)
                    if (isEvening == true)
                    {
                        message.Body += string.Format("Печать вечерней переоценки в {0} было выполнено по заданию на печать {1}. Было напечатано страниц {2} с компьютера {3} на принтер {4}",
                            eventInstance.TimeCreated.ToString(), eventInstance.Properties[0].Value, eventInstance.Properties[7].Value, eventInstance.Properties[3].Value,
                            eventInstance.Properties[4].Value) + Environment.NewLine;
                    }
                    else
                    {
                        message.Body += string.Format("Печать утренней переоценки в {0} было выполнено по заданию на печать {1}. Было напечатано страниц {2} с компьютера {3} на принтер {4}",
                            eventInstance.TimeCreated.ToString(), eventInstance.Properties[0].Value, eventInstance.Properties[7].Value, eventInstance.Properties[3].Value,
                            eventInstance.Properties[4].Value) + Environment.NewLine;
                    }

                
            }


            message.ToRecipients.Add(string.Format("sysadminS{0}@samberi.com", config.Gm));
//            message.CcRecipients.Add("romanov_a@samberi.com");
            message.Save();
#if DEBUG
#else
            //message.SendAndSaveCopy();
            message.Send();
#endif
            var s = "";
            for (int i = 0; i < message.ToRecipients.Count; i++)
            {
                s += message.ToRecipients[i] + ", ";
            }
            s = s.TrimEnd(' ').TrimEnd(',');
            logger.Trace("Письма отправленына на {0}", s);
        }

        private void timerMorning_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour == config.TimeMorning.Hour &&
                DateTime.Now.Minute == config.TimeMorning.Minute)
            {
                timerMorning.Enabled = false;
                logger.Trace("Сработал утренний таймер в {}", DateTime.Now);
                Clicker(false);
                timerMorning.Enabled = true;
            }
        }

        private void timerEvening_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.Hour == config.TimeEvening.Hour &&
                DateTime.Now.Minute == config.TimeEvening.Minute)
            {
                timerEvening.Enabled = false;
                logger.Trace("Сработал вечерний таймер в {}", DateTime.Now);
                Clicker(true);
                timerEvening.Enabled = true;
            }
        }
    }
}
