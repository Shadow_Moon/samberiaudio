﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoOvervalue
{
    [Serializable]
    public class Config
    {
        private double multiply;
        private int gm;
        private DateTime timeMorning, timeEvening;

        public Config()
        { }

        public int Gm { get => gm; set => gm = value; }
        public double Multiply { get => multiply; set => multiply = value; }
        public DateTime TimeEvening { get => timeEvening; set => timeEvening = value; }
        public DateTime TimeMorning { get => timeMorning; set => timeMorning = value; }
    }
}
