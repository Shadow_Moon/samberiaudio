﻿namespace AdminSamberiAudio
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConnect = new System.Windows.Forms.Label();
            this.backgroundWorkerTcpClient = new System.ComponentModel.BackgroundWorker();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.volumeSliderFile = new NAudio.Gui.VolumeSlider();
            this.dateTimePickerLast = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxVolume = new System.Windows.Forms.TextBox();
            this.labelLenghtFile = new System.Windows.Forms.Label();
            this.labelPathFile = new System.Windows.Forms.Label();
            this.textBoxIndex = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelLenght = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.labelPath = new System.Windows.Forms.Label();
            this.labelIndex = new System.Windows.Forms.Label();
            this.groupBoxTime = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxAllDays = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxLastStart = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxFrequency = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maskedTextBoxFirstStart = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkedListBoxDays = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxMinPause = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.volumeSliderMain = new NAudio.Gui.VolumeSlider();
            this.labelVersion = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelConnect
            // 
            this.labelConnect.AutoSize = true;
            this.labelConnect.Location = new System.Drawing.Point(13, 13);
            this.labelConnect.Name = "labelConnect";
            this.labelConnect.Size = new System.Drawing.Size(75, 13);
            this.labelConnect.TabIndex = 1;
            this.labelConnect.Text = "Выберите ГМ";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(16, 30);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.comboBox1.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(16, 70);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(423, 238);
            this.listBox1.TabIndex = 22;
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_SelectedValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.volumeSliderFile);
            this.groupBox1.Controls.Add(this.dateTimePickerLast);
            this.groupBox1.Controls.Add(this.dateTimePickerStart);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelLenght);
            this.groupBox1.Controls.Add(this.labelVolume);
            this.groupBox1.Controls.Add(this.labelPath);
            this.groupBox1.Controls.Add(this.labelIndex);
            this.groupBox1.Location = new System.Drawing.Point(445, 64);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 254);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Конфиг файла";
            // 
            // volumeSliderFile
            // 
            this.volumeSliderFile.Enabled = false;
            this.volumeSliderFile.Location = new System.Drawing.Point(6, 18);
            this.volumeSliderFile.Name = "volumeSliderFile";
            this.volumeSliderFile.Size = new System.Drawing.Size(334, 23);
            this.volumeSliderFile.TabIndex = 14;
            // 
            // dateTimePickerLast
            // 
            this.dateTimePickerLast.Enabled = false;
            this.dateTimePickerLast.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLast.Location = new System.Drawing.Point(182, 212);
            this.dateTimePickerLast.Name = "dateTimePickerLast";
            this.dateTimePickerLast.Size = new System.Drawing.Size(157, 20);
            this.dateTimePickerLast.TabIndex = 13;
            this.dateTimePickerLast.Value = new System.DateTime(2018, 4, 1, 0, 0, 0, 0);
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.Enabled = false;
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(5, 212);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(152, 20);
            this.dateTimePickerStart.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(179, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Последний день запуска";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Первый день запуска";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBoxVolume);
            this.panel1.Controls.Add(this.labelLenghtFile);
            this.panel1.Controls.Add(this.labelPathFile);
            this.panel1.Controls.Add(this.textBoxIndex);
            this.panel1.Location = new System.Drawing.Point(132, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(207, 117);
            this.panel1.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 16);
            this.label11.TabIndex = 8;
            this.label11.Text = "Название";
            // 
            // textBoxVolume
            // 
            this.textBoxVolume.Enabled = false;
            this.textBoxVolume.Location = new System.Drawing.Point(4, 49);
            this.textBoxVolume.Name = "textBoxVolume";
            this.textBoxVolume.Size = new System.Drawing.Size(100, 20);
            this.textBoxVolume.TabIndex = 7;
            // 
            // labelLenghtFile
            // 
            this.labelLenghtFile.AutoSize = true;
            this.labelLenghtFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLenghtFile.Location = new System.Drawing.Point(1, 69);
            this.labelLenghtFile.Name = "labelLenghtFile";
            this.labelLenghtFile.Size = new System.Drawing.Size(146, 16);
            this.labelLenghtFile.TabIndex = 6;
            this.labelLenghtFile.Text = "Длительность файла";
            // 
            // labelPathFile
            // 
            this.labelPathFile.AutoSize = true;
            this.labelPathFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPathFile.Location = new System.Drawing.Point(3, 29);
            this.labelPathFile.Name = "labelPathFile";
            this.labelPathFile.Size = new System.Drawing.Size(105, 16);
            this.labelPathFile.TabIndex = 6;
            this.labelPathFile.Text = "Путь до файла";
            // 
            // textBoxIndex
            // 
            this.textBoxIndex.Enabled = false;
            this.textBoxIndex.Location = new System.Drawing.Point(4, 7);
            this.textBoxIndex.Name = "textBoxIndex";
            this.textBoxIndex.Size = new System.Drawing.Size(100, 20);
            this.textBoxIndex.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Название";
            // 
            // labelLenght
            // 
            this.labelLenght.AutoSize = true;
            this.labelLenght.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLenght.Location = new System.Drawing.Point(6, 129);
            this.labelLenght.Name = "labelLenght";
            this.labelLenght.Size = new System.Drawing.Size(100, 16);
            this.labelLenght.TabIndex = 3;
            this.labelLenght.Text = "Длительность";
            // 
            // labelVolume
            // 
            this.labelVolume.AutoSize = true;
            this.labelVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVolume.Location = new System.Drawing.Point(6, 109);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(76, 16);
            this.labelVolume.TabIndex = 2;
            this.labelVolume.Text = "Громкость";
            // 
            // labelPath
            // 
            this.labelPath.AutoSize = true;
            this.labelPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPath.Location = new System.Drawing.Point(6, 89);
            this.labelPath.Name = "labelPath";
            this.labelPath.Size = new System.Drawing.Size(40, 16);
            this.labelPath.TabIndex = 1;
            this.labelPath.Text = "Путь";
            // 
            // labelIndex
            // 
            this.labelIndex.AutoSize = true;
            this.labelIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIndex.Location = new System.Drawing.Point(6, 69);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(56, 16);
            this.labelIndex.TabIndex = 0;
            this.labelIndex.Text = "Индекс";
            // 
            // groupBoxTime
            // 
            this.groupBoxTime.Controls.Add(this.label12);
            this.groupBoxTime.Controls.Add(this.checkBoxAllDays);
            this.groupBoxTime.Controls.Add(this.maskedTextBoxLastStart);
            this.groupBoxTime.Controls.Add(this.label5);
            this.groupBoxTime.Controls.Add(this.textBoxFrequency);
            this.groupBoxTime.Controls.Add(this.label4);
            this.groupBoxTime.Controls.Add(this.maskedTextBoxFirstStart);
            this.groupBoxTime.Controls.Add(this.label3);
            this.groupBoxTime.Controls.Add(this.checkedListBoxDays);
            this.groupBoxTime.Controls.Add(this.label2);
            this.groupBoxTime.Location = new System.Drawing.Point(796, 64);
            this.groupBoxTime.Name = "groupBoxTime";
            this.groupBoxTime.Size = new System.Drawing.Size(321, 254);
            this.groupBoxTime.TabIndex = 25;
            this.groupBoxTime.TabStop = false;
            this.groupBoxTime.Text = "Настройка периодичности";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(113, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(197, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Будет проигрываться раз в ХХ минут";
            // 
            // checkBoxAllDays
            // 
            this.checkBoxAllDays.AutoSize = true;
            this.checkBoxAllDays.Location = new System.Drawing.Point(10, 160);
            this.checkBoxAllDays.Name = "checkBoxAllDays";
            this.checkBoxAllDays.Size = new System.Drawing.Size(66, 17);
            this.checkBoxAllDays.TabIndex = 10;
            this.checkBoxAllDays.Text = "Все дни";
            this.checkBoxAllDays.UseVisualStyleBackColor = true;
            this.checkBoxAllDays.Visible = false;
            // 
            // maskedTextBoxLastStart
            // 
            this.maskedTextBoxLastStart.Enabled = false;
            this.maskedTextBoxLastStart.Location = new System.Drawing.Point(203, 44);
            this.maskedTextBoxLastStart.Mask = "90:00";
            this.maskedTextBoxLastStart.Name = "maskedTextBoxLastStart";
            this.maskedTextBoxLastStart.Size = new System.Drawing.Size(37, 20);
            this.maskedTextBoxLastStart.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(203, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Последний запуск";
            // 
            // textBoxFrequency
            // 
            this.textBoxFrequency.Enabled = false;
            this.textBoxFrequency.Location = new System.Drawing.Point(230, 69);
            this.textBoxFrequency.Name = "textBoxFrequency";
            this.textBoxFrequency.Size = new System.Drawing.Size(37, 20);
            this.textBoxFrequency.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(113, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Частота (х раз день)";
            // 
            // maskedTextBoxFirstStart
            // 
            this.maskedTextBoxFirstStart.Enabled = false;
            this.maskedTextBoxFirstStart.Location = new System.Drawing.Point(114, 44);
            this.maskedTextBoxFirstStart.Mask = "90:00";
            this.maskedTextBoxFirstStart.Name = "maskedTextBoxFirstStart";
            this.maskedTextBoxFirstStart.Size = new System.Drawing.Size(37, 20);
            this.maskedTextBoxFirstStart.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Первый запуск";
            // 
            // checkedListBoxDays
            // 
            this.checkedListBoxDays.Enabled = false;
            this.checkedListBoxDays.FormattingEnabled = true;
            this.checkedListBoxDays.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскресенье"});
            this.checkedListBoxDays.Location = new System.Drawing.Point(6, 44);
            this.checkedListBoxDays.Name = "checkedListBoxDays";
            this.checkedListBoxDays.Size = new System.Drawing.Size(94, 109);
            this.checkedListBoxDays.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Дни";
            // 
            // textBoxMinPause
            // 
            this.textBoxMinPause.Enabled = false;
            this.textBoxMinPause.Location = new System.Drawing.Point(946, 21);
            this.textBoxMinPause.Name = "textBoxMinPause";
            this.textBoxMinPause.Size = new System.Drawing.Size(37, 20);
            this.textBoxMinPause.TabIndex = 28;
            this.textBoxMinPause.Text = "1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(791, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Минимальная пауза (минут)";
            // 
            // volumeSliderMain
            // 
            this.volumeSliderMain.Enabled = false;
            this.volumeSliderMain.Location = new System.Drawing.Point(445, 28);
            this.volumeSliderMain.Name = "volumeSliderMain";
            this.volumeSliderMain.Size = new System.Drawing.Size(345, 23);
            this.volumeSliderMain.TabIndex = 26;
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(144, 37);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(44, 13);
            this.labelVersion.TabIndex = 29;
            this.labelVersion.Text = "Версия";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 371);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.textBoxMinPause);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.volumeSliderMain);
            this.Controls.Add(this.groupBoxTime);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelConnect);
            this.Name = "Form1";
            this.Text = "Админка";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxTime.ResumeLayout(false);
            this.groupBoxTime.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelConnect;
        private System.ComponentModel.BackgroundWorker backgroundWorkerTcpClient;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dateTimePickerLast;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxVolume;
        private System.Windows.Forms.Label labelLenghtFile;
        private System.Windows.Forms.Label labelPathFile;
        private System.Windows.Forms.TextBox textBoxIndex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelLenght;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.Label labelPath;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.GroupBox groupBoxTime;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBoxAllDays;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxLastStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxFrequency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxFirstStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckedListBox checkedListBoxDays;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxMinPause;
        private System.Windows.Forms.Label label6;
        private NAudio.Gui.VolumeSlider volumeSliderMain;
        private NAudio.Gui.VolumeSlider volumeSliderFile;
        private System.Windows.Forms.Label labelVersion;
    }
}

