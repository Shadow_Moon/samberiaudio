﻿using System;
using System.Collections.Generic;

namespace AdminSamberiAudio.SQL
{
    public partial class Connected
    {
        public Connected()
        {
            PlayedSongs = new HashSet<PlayedSongs>();
            Songs = new HashSet<Songs>();
        }

        public int Id { get; set; }
        public int Name { get; set; }
        public bool? IsConnected { get; set; }
        public DateTime? Time { get; set; }
        public float? Volume { get; set; }
        public int? MinPause { get; set; }
        public string Version { get; set; }
        public ICollection<PlayedSongs> PlayedSongs { get; set; }
        public ICollection<Songs> Songs { get; set; }
    }
}
