﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AdminSamberiAudio.SQL
{
    public partial class postgresContext : DbContext
    {
        public postgresContext()
        {
        }

        public postgresContext(DbContextOptions<postgresContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Connected> Connected { get; set; }
        public virtual DbSet<PlayedSongs> PlayedSongs { get; set; }
        public virtual DbSet<Songs> Songs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
#if DEBUG
                optionsBuilder.UseNpgsql("Host=localhost;Username=SamberiAudio;Password=123456;Database=postgres");
#else
                optionsBuilder.UseNpgsql("Host=sdiv-007.samberi.com;Username=SamberiAudio;Password=123456;Database=postgres");
#endif
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Connected>(entity =>
            {
                entity.ToTable("Connected", "SamberiAudio");

                entity.HasIndex(e => e.Name)
                    .HasName("_name")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsConnected).HasColumnName("isConnected");

                entity.Property(e => e.MinPause)
                    .HasColumnName("min_pause")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Time)
                    .HasColumnName("time")
                    .HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.Volume)
                    .HasColumnName("volume")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<PlayedSongs>(entity =>
            {
                entity.ToTable("Played_Songs", "SamberiAudio");

                entity.Property(e => e.Gm).HasColumnName("GM");

                entity.Property(e => e.IdSong).HasColumnName("ID_Song");

                entity.Property(e => e.LastTime).HasColumnType("timestamp with time zone");

                entity.HasOne(d => d.GmNavigation)
                    .WithMany(p => p.PlayedSongs)
                    .HasPrincipalKey(p => p.Name)
                    .HasForeignKey(d => d.Gm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Played_Songs_Connected_1");

                entity.HasOne(d => d.IdSongNavigation)
                    .WithMany(p => p.PlayedSongs)
                    .HasForeignKey(d => d.IdSong)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Played_Songs_ID_Song_fkey");
            });

            modelBuilder.Entity<Songs>(entity =>
            {
                entity.ToTable("Songs", "SamberiAudio");

                entity.Property(e => e.Days)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FirstAdd).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.FirstDayStart).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.FirstTimeStart).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.Gm).HasColumnName("GM");

                entity.Property(e => e.LastDayStart).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.LastTimeStart).HasColumnType("timestamp(6) without time zone");

                entity.Property(e => e.Path)
                    .IsRequired()
                    .HasMaxLength(512);

                entity.Property(e => e.Song)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.GmNavigation)
                    .WithMany(p => p.Songs)
                    .HasPrincipalKey(p => p.Name)
                    .HasForeignKey(d => d.Gm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Songs_Connected_1");
            });
        }
    }
}
