﻿using System;
using System.Collections.Generic;

namespace AdminSamberiAudio.SQL
{
    public partial class Songs
    {
        public Songs()
        {
            PlayedSongs = new HashSet<PlayedSongs>();
        }

        public string Song { get; set; }
        public int Id { get; set; }
        public int Frequency { get; set; }
        public DateTime FirstTimeStart { get; set; }
        public DateTime LastTimeStart { get; set; }
        public DateTime FirstDayStart { get; set; }
        public DateTime LastDayStart { get; set; }
        public string Path { get; set; }
        public float Volume { get; set; }
        public int Gm { get; set; }
        public DateTime FirstAdd { get; set; }
        public int LenghtSeconds { get; set; }
        public string Days { get; set; }

        public Connected GmNavigation { get; set; }
        public ICollection<PlayedSongs> PlayedSongs { get; set; }
    }
}
