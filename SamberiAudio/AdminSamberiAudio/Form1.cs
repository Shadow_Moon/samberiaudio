﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdminSamberiAudio
{
    public partial class Form1 : Form
    {
        public static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        SQL.Songs[] songs;
       // private TcpClient client;
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //backgroundWorkerTcpClient.RunWorkerAsync();
            FillComboGM();
        }

        private void FillComboGM()
        {
            using (var db = new SQL.postgresContext())
            {
                var tmp = db.Connected.ToArray().OrderBy(s => s.Name);
                foreach (var t in tmp)
                {
                    comboBox1.Items.Add(t.Name);
                }
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            FillComboGM();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            using (var db = new SQL.postgresContext())
            {
                volumeSliderMain.Volume = (float)db.Connected.Where(s => s.Name == (int)comboBox1.SelectedItem).SingleOrDefault().Volume;
                textBoxMinPause.Text = db.Connected.Where(s => s.Name == (int)comboBox1.SelectedItem).SingleOrDefault().MinPause.ToString();
                songs = db.Songs.Where(s => s.Gm == (int)comboBox1.SelectedItem && s.LastDayStart > DateTime.Now).ToArray();
                labelVersion.Text = db.Connected.Where(s => s.Name == (int)comboBox1.SelectedItem).SingleOrDefault().Version;
            }
            foreach (var song in songs)
            {
                listBox1.Items.Add(song.Song);
            }
        }

        private void listBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            var song = songs.Where(s => s.Song == (string)listBox1.SelectedItem).SingleOrDefault();
            if (listBox1.SelectedIndex < 0)
            {
                return;
            }
            textBoxIndex.Text = listBox1.SelectedIndex.ToString();
            labelPathFile.Text = song.Path;
            label11.Text = song.Song;
            textBoxVolume.Text = Convert.ToString(20 * (float)Math.Log10((float)song.Volume)) + " dB";
            volumeSliderFile.Volume = song.Volume;
            dateTimePickerStart.Value = ((DateTime)song.FirstDayStart).Date;
            try
            {
                dateTimePickerLast.Value = ((DateTime)song.LastDayStart).Date;
            }
            catch (ArgumentOutOfRangeException ee)
            {
                dateTimePickerLast.Value = ((DateTime)song.LastDayStart).AddDays(-1);
            }
            labelLenghtFile.Text = String.Format("{0:00}:{1:00}", (int)(song.LenghtSeconds/60), song.LenghtSeconds-((int)(song.LenghtSeconds/60))*60);
            LoadCheckedDays(song.Days);
            LoadTime(song);

            DateTime dtStart, dtLast;
            dtStart = saveDateTime(maskedTextBoxFirstStart.Text);
            dtLast = saveDateTime(maskedTextBoxLastStart.Text);
            switch (textBoxFrequency.Text)
            {
                case "1":
                    {
                        label12.Text = string.Format("Будет проигрываться только в {0}:{1}",
                            dtStart.Hour, dtStart.Minute);
                        break;
                    }
                case "2":
                    {
                        label12.Text = string.Format("Будет проигрываться только в {0}:{1} \r\n" +
                                                     "                                               и в {2}:{3}",
                            dtStart.Hour, dtStart.Minute,
                            dtLast.Hour, dtLast.Minute);
                        break;
                    }
                default:
                    {
                        if (textBoxFrequency.Text != "" && maskedTextBoxFirstStart.Text != ":" && maskedTextBoxLastStart.Text != ":")
                        {
                            label12.Text = string.Format("Будет проигрываться раз в {0} минут",
                                (dtLast.Hour * 60 + dtLast.Minute - dtStart.Hour * 60 + dtStart.Minute) / Convert.ToInt32(textBoxFrequency.Text));
                        }
                        break;
                    }
            }
        }

        private DateTime saveDateTime(string str)
        {
            DateTime dt;
            string[] tmp = new string[2];
            tmp = str.Split(':');
            if (tmp[0] == " " || tmp[1] == "")
            {
                dt = new DateTime(1, 1, 1, 0, 0, 0);
                return dt;
            }
            dt = new DateTime(1, 1, 1, Convert.ToInt32(tmp[0]), Convert.ToInt32(tmp[1]), 0);
            return dt;
        }

        private void LoadTime(SQL.Songs song)
        {            
            try
            {
                maskedTextBoxFirstStart.Text = LoadDateTime((DateTime)song.FirstTimeStart);
                maskedTextBoxLastStart.Text = LoadDateTime((DateTime)song.LastTimeStart);
                textBoxFrequency.Text = song.Frequency.ToString();
            }
            catch (NullReferenceException)
            {

            }
            catch (IndexOutOfRangeException)
            {
                maskedTextBoxFirstStart.Text = "";
                maskedTextBoxLastStart.Text = "";
                textBoxFrequency.Text = "";
            }

        }

        private string LoadDateTime(DateTime dt)
        {
            string str;
            if (dt.Hour <= 9)
            {
                str = "0" + dt.Hour.ToString();
            }
            else
            {
                str = dt.Hour.ToString();
            }
            if (dt.Minute <= 9)
            {
                str += ":" + "0" + dt.Minute.ToString();
            }
            else
            {
                str += ":" + dt.Minute.ToString();
            }
            return str;
        }

        private void LoadCheckedDays(string str)
        {
            try
            {
                for (int j = 0; j < checkedListBoxDays.Items.Count; j++)
                {
                    checkedListBoxDays.SetItemChecked(j, Convert.ToBoolean(Convert.ToInt16(str[j].ToString())));
                }
            }
            catch (NullReferenceException)
            {

            }
            catch (IndexOutOfRangeException)
            {
                for (int j = 0; j < checkedListBoxDays.Items.Count; j++)
                {
                    checkedListBoxDays.SetItemChecked(j, false);
                }
            }

        }
    }
}
