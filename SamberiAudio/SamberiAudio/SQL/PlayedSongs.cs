﻿using System;
using System.Collections.Generic;

namespace SamberiAudio.SQL
{
    public partial class PlayedSongs
    {
        public int Gm { get; set; }
        public int IdSong { get; set; }
        public DateTime? LastTime { get; set; }
        public int Id { get; set; }
        public bool? NowPlayed { get; set; }

        public Connected GmNavigation { get; set; }
        public Songs IdSongNavigation { get; set; }
    }
}
