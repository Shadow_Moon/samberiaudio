﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SamberiAudio
{

    [Serializable]
    public class Config
    {
        private Song[] song;
        private Song[] songFON;

        private const string CONFPATH = "config.ini";

        private Single mainVolume;
        private int minPause;
        private int numberGiper;

        private bool silentCheckBox;
        private DateTime silentStart;
        private DateTime silentEnd;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public Config()
        { }

        public Config(int index)
        {
            song = new Song[index];
            for (int i = 0; i < index; i++)
            {
                song[i] = new Song(null, 0, null, new DateTime(1, 1, 1, 1, 1, 1, 0), new DateTime(1, 1, 1, 1, 1, 1, 0), 0, null);
            }
            songFON = new Song[index];
            for (int i = 0; i < index; i++)
            {
                songFON[i] = new Song(null, 0, null, new DateTime(1, 1, 1, 1, 1, 1, 0), new DateTime(1, 1, 1, 1, 1, 1, 0), 0, null);
            }
        }


        public void ResizeIncrement ()
        {
            Array.Resize(ref song, song.Length + 1);
            song[song.Length - 1] = new Song(null, 0, null, new DateTime(1, 1, 1, 1, 1, 1, 0), new DateTime(1, 1, 1, 1, 1, 1, 0), 0, null);
        }

        public void ResizeIncrementАFON(int count)
        {
            if (songFON == null)
                songFON = new Song[0];
            Array.Resize(ref songFON, songFON.Length + count);
            for (int i = songFON.Length; i < SongFON.Length + count; i++)
            {
                songFON[songFON.Length - 1] = new Song(null, 0, null, new DateTime(1, 1, 1, 1, 1, 1, 0), new DateTime(1, 1, 1, 1, 1, 1, 0), 0, null);
            }
        }

        public void DeleteSong(int index)
        {
            var count = 0;
            var countTmp = 0;
            Song[] tmp = new Song[song.Length - 1];
            for (int i = 0; i < song.Length; i++)
            {
                if (i != index)
                {
                    tmp[countTmp] = song[count];
                    count++;
                    countTmp++;
                }
                else
                { count++; }
            }
            song = tmp;
        }

        public float MainVolume
        {
            get
            {
                return mainVolume;
            }

            set
            {
                mainVolume = value;
            }
        } 

        public int MinPause
        {
            get
            {
                return minPause;
            }

            set
            {
                minPause = value;
            }
        }

        public void SaveSQL()
        {
            try
            {
                using (var db = new SQL.postgresContext())
                {
                    foreach (var confsong in Song)
                    {
                        if (db.Songs.Where(s => s.Song == confsong.Name && s.Gm == this.numberGiper).Count() == 0)
                        {
                            db.Songs.Add(SaveNewSongSQL(confsong));
                        }
                        else
                        {
                            SQL.Songs update = db.Songs.Where(s => s.Song == confsong.Name && s.Gm == this.NumberGiper).SingleOrDefault();
                            update = UpdateSongSQL(confsong, update);
                            update.Gm = db.Connected.Where(s => s.Name == this.NumberGiper).SingleOrDefault().Name;
                        }
                    }
                    var tmp = db.Connected.Where(s => s.Name == this.numberGiper).SingleOrDefault();
                    tmp.Volume = this.mainVolume;
                    tmp.MinPause = this.minPause;
                    var count = db.SaveChanges();
                }
            }
            catch (System.Net.Sockets.SocketException)
            {
                logger.Info("Нет подключения к серверу SQL. Настройки не были сохранены");
            }
            catch (System.TimeoutException)
            {
                logger.Info("Нет подключения к серверу SQL по таймауту. Настройки не были сохранены");
            }
            catch (System.InvalidOperationException)
            {
                logger.Info("Нет подключения к серверу SQL. Настройки не были сохранены. System.InvalidOperationException.");
            }
        }

    

        public int NumberGiper { get => numberGiper; set => numberGiper = value; }
        public bool SilentCheckBox { get => silentCheckBox; set => silentCheckBox = value; }
        public DateTime SilentStart { get => silentStart; set => silentStart = value; }
        public DateTime SilentEnd { get => silentEnd; set => silentEnd = value; }
        public Song[] Song { get { return song; } set { song = value; } }
        public Song[] SongFON { get {return songFON; } set { songFON = value; } }

        private SQL.Songs SaveNewSongSQL(SamberiAudio.Song confsong)
        {
            var newsong = new SQL.Songs();
            newsong.Song = confsong.Name;
            newsong.Frequency = confsong.Frequency;
            newsong.FirstDayStart = confsong.FirstDayStart;
            newsong.FirstTimeStart = confsong.FirstTimeStart;
            newsong.LastDayStart = confsong.LastDayStart;
            newsong.LastTimeStart = confsong.LastTimeStart;
            newsong.Path = confsong.Path;
            newsong.Volume = confsong.Volume;
            newsong.Gm = this.numberGiper;
            newsong.FirstAdd = DateTime.Now;
            newsong.Days = confsong.Days;
            var tmp = new NAudio.Wave.AudioFileReader(confsong.Path);
            newsong.LenghtSeconds = (int)tmp.TotalTime.TotalSeconds;
            tmp.Dispose();
            return newsong;
        }

        private SQL.Songs UpdateSongSQL(SamberiAudio.Song confsong, SQL.Songs updatesong)
        {
            updatesong.Song = confsong.Name;
            updatesong.Frequency = confsong.Frequency;
            updatesong.FirstDayStart = confsong.FirstDayStart;
            updatesong.FirstTimeStart = confsong.FirstTimeStart;
            updatesong.LastDayStart = confsong.LastDayStart;
            updatesong.LastTimeStart = confsong.LastTimeStart;
            updatesong.Path = confsong.Path;
            updatesong.Volume = confsong.Volume;
            updatesong.Days = confsong.Days;
            if (updatesong.FirstAdd == null)
                updatesong.FirstAdd = DateTime.Now;
            if (updatesong.LenghtSeconds == null)
            {
                var tmp = new NAudio.Wave.AudioFileReader(confsong.Path);
                updatesong.LenghtSeconds = (int)tmp.TotalTime.TotalSeconds;
                tmp.Dispose();
            }
            return updatesong;
        }
    }
}
