﻿namespace SamberiAudio
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.contextMenuStripListBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button3 = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.timerRenewSongTrackBar = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.labelCurrentTime = new System.Windows.Forms.ToolStripLabel();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.labelTotalTime = new System.Windows.Forms.ToolStripLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрграммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.багтрекерToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dateTimePickerLast = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.volumeSliderFile = new NAudio.Gui.VolumeSlider();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxVolume = new System.Windows.Forms.TextBox();
            this.labelLenghtFile = new System.Windows.Forms.Label();
            this.labelPathFile = new System.Windows.Forms.Label();
            this.textBoxIndex = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelLenght = new System.Windows.Forms.Label();
            this.labelVolume = new System.Windows.Forms.Label();
            this.labelPath = new System.Windows.Forms.Label();
            this.labelIndex = new System.Windows.Forms.Label();
            this.buttonSaveFile = new System.Windows.Forms.Button();
            this.buttonSaveConfig = new System.Windows.Forms.Button();
            this.buttonLoadConfig = new System.Windows.Forms.Button();
            this.groupBoxTime = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBoxAllDays = new System.Windows.Forms.CheckBox();
            this.maskedTextBoxLastStart = new System.Windows.Forms.MaskedTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxFrequency = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maskedTextBoxFirstStart = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkedListBoxDays = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.textBoxMinPause = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.button4 = new System.Windows.Forms.Button();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.timerResetSheduler = new System.Windows.Forms.Timer(this.components);
            this.volumeSliderMain = new NAudio.Gui.VolumeSlider();
            this.backgroundWorkerTcpClient = new System.ComponentModel.BackgroundWorker();
            this.labelConnect = new System.Windows.Forms.Label();
            this.labelCurrentSong = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.buttonDEL = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.buttomUP = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.maskedTextSilentStart = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextSilentEnd = new System.Windows.Forms.MaskedTextBox();
            this.checkBoxSilent = new System.Windows.Forms.CheckBox();
            this.timerSilentMode = new System.Windows.Forms.Timer(this.components);
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.button8 = new System.Windows.Forms.Button();
            this.contextMenuStripListBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxTime.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Запуск";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 253);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Стоп";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBox1
            // 
            this.listBox1.ContextMenuStrip = this.contextMenuStripListBox;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(296, 96);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(423, 238);
            this.listBox1.TabIndex = 2;
            this.listBox1.Click += new System.EventHandler(this.listBox1_Click);
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.listBox1_Click);
            this.listBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.listBox1_MouseUp);
            // 
            // contextMenuStripListBox
            // 
            this.contextMenuStripListBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.удалитьToolStripMenuItem});
            this.contextMenuStripListBox.Name = "contextMenuStripListBox";
            this.contextMenuStripListBox.Size = new System.Drawing.Size(119, 26);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(303, 52);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(98, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "Добавить файл";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Enabled = false;
            this.trackBar1.Location = new System.Drawing.Point(12, 415);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(734, 45);
            this.trackBar1.TabIndex = 6;
            this.trackBar1.TickFrequency = 5;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // timerRenewSongTrackBar
            // 
            this.timerRenewSongTrackBar.Enabled = true;
            this.timerRenewSongTrackBar.Interval = 500;
            this.timerRenewSongTrackBar.Tick += new System.EventHandler(this.TimerRenewTrackBar_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.labelCurrentTime,
            this.toolStripLabel2,
            this.labelTotalTime});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1436, 25);
            this.toolStrip1.TabIndex = 8;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(77, 22);
            this.toolStripLabel1.Text = "Current Time";
            // 
            // labelCurrentTime
            // 
            this.labelCurrentTime.Name = "labelCurrentTime";
            this.labelCurrentTime.Size = new System.Drawing.Size(34, 22);
            this.labelCurrentTime.Text = "00:00";
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(63, 22);
            this.toolStripLabel2.Text = "Total Time";
            // 
            // labelTotalTime
            // 
            this.labelTotalTime.Name = "labelTotalTime";
            this.labelTotalTime.Size = new System.Drawing.Size(34, 22);
            this.labelTotalTime.Text = "00:00";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1436, 24);
            this.menuStrip1.TabIndex = 9;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрграммеToolStripMenuItem,
            this.багтрекерToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрграммеToolStripMenuItem
            // 
            this.оПрграммеToolStripMenuItem.Name = "оПрграммеToolStripMenuItem";
            this.оПрграммеToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.оПрграммеToolStripMenuItem.Text = "О программе";
            this.оПрграммеToolStripMenuItem.Click += new System.EventHandler(this.оПрграммеToolStripMenuItem_Click);
            // 
            // багтрекерToolStripMenuItem
            // 
            this.багтрекерToolStripMenuItem.Name = "багтрекерToolStripMenuItem";
            this.багтрекерToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.багтрекерToolStripMenuItem.Text = "Багтрекер";
            this.багтрекерToolStripMenuItem.Click += new System.EventHandler(this.багтрекерToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dateTimePickerLast);
            this.groupBox1.Controls.Add(this.dateTimePickerStart);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.volumeSliderFile);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.labelLenght);
            this.groupBox1.Controls.Add(this.labelVolume);
            this.groupBox1.Controls.Add(this.labelPath);
            this.groupBox1.Controls.Add(this.labelIndex);
            this.groupBox1.Location = new System.Drawing.Point(751, 81);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(345, 254);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Конфиг файла";
            // 
            // dateTimePickerLast
            // 
            this.dateTimePickerLast.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerLast.Location = new System.Drawing.Point(182, 212);
            this.dateTimePickerLast.Name = "dateTimePickerLast";
            this.dateTimePickerLast.Size = new System.Drawing.Size(157, 20);
            this.dateTimePickerLast.TabIndex = 13;
            this.dateTimePickerLast.Value = new System.DateTime(2018, 4, 1, 0, 0, 0, 0);
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerStart.Location = new System.Drawing.Point(5, 212);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(152, 20);
            this.dateTimePickerStart.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(179, 196);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(134, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "Последний день запуска";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 196);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Первый день запуска";
            // 
            // volumeSliderFile
            // 
            this.volumeSliderFile.Location = new System.Drawing.Point(5, 19);
            this.volumeSliderFile.Name = "volumeSliderFile";
            this.volumeSliderFile.Size = new System.Drawing.Size(334, 23);
            this.volumeSliderFile.TabIndex = 7;
            this.volumeSliderFile.VolumeChanged += new System.EventHandler(this.volumeSliderFile_VolumeChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.textBoxVolume);
            this.panel1.Controls.Add(this.labelLenghtFile);
            this.panel1.Controls.Add(this.labelPathFile);
            this.panel1.Controls.Add(this.textBoxIndex);
            this.panel1.Location = new System.Drawing.Point(132, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(207, 117);
            this.panel1.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(3, 89);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 16);
            this.label11.TabIndex = 8;
            this.label11.Text = "Название";
            // 
            // textBoxVolume
            // 
            this.textBoxVolume.Enabled = false;
            this.textBoxVolume.Location = new System.Drawing.Point(4, 49);
            this.textBoxVolume.Name = "textBoxVolume";
            this.textBoxVolume.Size = new System.Drawing.Size(100, 20);
            this.textBoxVolume.TabIndex = 7;
            // 
            // labelLenghtFile
            // 
            this.labelLenghtFile.AutoSize = true;
            this.labelLenghtFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLenghtFile.Location = new System.Drawing.Point(1, 69);
            this.labelLenghtFile.Name = "labelLenghtFile";
            this.labelLenghtFile.Size = new System.Drawing.Size(146, 16);
            this.labelLenghtFile.TabIndex = 6;
            this.labelLenghtFile.Text = "Длительность файла";
            // 
            // labelPathFile
            // 
            this.labelPathFile.AutoSize = true;
            this.labelPathFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPathFile.Location = new System.Drawing.Point(3, 29);
            this.labelPathFile.Name = "labelPathFile";
            this.labelPathFile.Size = new System.Drawing.Size(105, 16);
            this.labelPathFile.TabIndex = 6;
            this.labelPathFile.Text = "Путь до файла";
            // 
            // textBoxIndex
            // 
            this.textBoxIndex.Enabled = false;
            this.textBoxIndex.Location = new System.Drawing.Point(4, 7);
            this.textBoxIndex.Name = "textBoxIndex";
            this.textBoxIndex.Size = new System.Drawing.Size(100, 20);
            this.textBoxIndex.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(6, 149);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "Название";
            // 
            // labelLenght
            // 
            this.labelLenght.AutoSize = true;
            this.labelLenght.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelLenght.Location = new System.Drawing.Point(6, 129);
            this.labelLenght.Name = "labelLenght";
            this.labelLenght.Size = new System.Drawing.Size(100, 16);
            this.labelLenght.TabIndex = 3;
            this.labelLenght.Text = "Длительность";
            // 
            // labelVolume
            // 
            this.labelVolume.AutoSize = true;
            this.labelVolume.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelVolume.Location = new System.Drawing.Point(6, 109);
            this.labelVolume.Name = "labelVolume";
            this.labelVolume.Size = new System.Drawing.Size(76, 16);
            this.labelVolume.TabIndex = 2;
            this.labelVolume.Text = "Громкость";
            // 
            // labelPath
            // 
            this.labelPath.AutoSize = true;
            this.labelPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPath.Location = new System.Drawing.Point(6, 89);
            this.labelPath.Name = "labelPath";
            this.labelPath.Size = new System.Drawing.Size(40, 16);
            this.labelPath.TabIndex = 1;
            this.labelPath.Text = "Путь";
            // 
            // labelIndex
            // 
            this.labelIndex.AutoSize = true;
            this.labelIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelIndex.Location = new System.Drawing.Point(6, 69);
            this.labelIndex.Name = "labelIndex";
            this.labelIndex.Size = new System.Drawing.Size(56, 16);
            this.labelIndex.TabIndex = 0;
            this.labelIndex.Text = "Индекс";
            // 
            // buttonSaveFile
            // 
            this.buttonSaveFile.Location = new System.Drawing.Point(1033, 323);
            this.buttonSaveFile.Name = "buttonSaveFile";
            this.buttonSaveFile.Size = new System.Drawing.Size(132, 23);
            this.buttonSaveFile.TabIndex = 6;
            this.buttonSaveFile.Text = "Сохранить настройки";
            this.buttonSaveFile.UseVisualStyleBackColor = true;
            this.buttonSaveFile.Click += new System.EventHandler(this.buttonSaveFile_Click);
            // 
            // buttonSaveConfig
            // 
            this.buttonSaveConfig.Location = new System.Drawing.Point(669, 359);
            this.buttonSaveConfig.Name = "buttonSaveConfig";
            this.buttonSaveConfig.Size = new System.Drawing.Size(128, 23);
            this.buttonSaveConfig.TabIndex = 11;
            this.buttonSaveConfig.Text = "Сохранить конфиг";
            this.buttonSaveConfig.UseVisualStyleBackColor = true;
            this.buttonSaveConfig.Click += new System.EventHandler(this.buttonSaveConfig_Click);
            // 
            // buttonLoadConfig
            // 
            this.buttonLoadConfig.Location = new System.Drawing.Point(807, 359);
            this.buttonLoadConfig.Name = "buttonLoadConfig";
            this.buttonLoadConfig.Size = new System.Drawing.Size(124, 23);
            this.buttonLoadConfig.TabIndex = 12;
            this.buttonLoadConfig.Text = "Загрузить конфиг";
            this.buttonLoadConfig.UseVisualStyleBackColor = true;
            this.buttonLoadConfig.Click += new System.EventHandler(this.buttonLoadConfig_Click);
            // 
            // groupBoxTime
            // 
            this.groupBoxTime.Controls.Add(this.label12);
            this.groupBoxTime.Controls.Add(this.checkBoxAllDays);
            this.groupBoxTime.Controls.Add(this.maskedTextBoxLastStart);
            this.groupBoxTime.Controls.Add(this.label5);
            this.groupBoxTime.Controls.Add(this.textBoxFrequency);
            this.groupBoxTime.Controls.Add(this.label4);
            this.groupBoxTime.Controls.Add(this.maskedTextBoxFirstStart);
            this.groupBoxTime.Controls.Add(this.label3);
            this.groupBoxTime.Controls.Add(this.checkedListBoxDays);
            this.groupBoxTime.Controls.Add(this.label2);
            this.groupBoxTime.Location = new System.Drawing.Point(1103, 81);
            this.groupBoxTime.Name = "groupBoxTime";
            this.groupBoxTime.Size = new System.Drawing.Size(321, 254);
            this.groupBoxTime.TabIndex = 14;
            this.groupBoxTime.TabStop = false;
            this.groupBoxTime.Text = "Настройка периодичности";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(113, 101);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(197, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Будет проигрываться раз в ХХ минут";
            // 
            // checkBoxAllDays
            // 
            this.checkBoxAllDays.AutoSize = true;
            this.checkBoxAllDays.Location = new System.Drawing.Point(10, 160);
            this.checkBoxAllDays.Name = "checkBoxAllDays";
            this.checkBoxAllDays.Size = new System.Drawing.Size(66, 17);
            this.checkBoxAllDays.TabIndex = 10;
            this.checkBoxAllDays.Text = "Все дни";
            this.checkBoxAllDays.UseVisualStyleBackColor = true;
            this.checkBoxAllDays.CheckedChanged += new System.EventHandler(this.checkBoxAllDays_CheckedChanged);
            // 
            // maskedTextBoxLastStart
            // 
            this.maskedTextBoxLastStart.Location = new System.Drawing.Point(203, 44);
            this.maskedTextBoxLastStart.Mask = "90:00";
            this.maskedTextBoxLastStart.Name = "maskedTextBoxLastStart";
            this.maskedTextBoxLastStart.Size = new System.Drawing.Size(37, 20);
            this.maskedTextBoxLastStart.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(203, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Последний запуск";
            // 
            // textBoxFrequency
            // 
            this.textBoxFrequency.Location = new System.Drawing.Point(230, 69);
            this.textBoxFrequency.Name = "textBoxFrequency";
            this.textBoxFrequency.Size = new System.Drawing.Size(37, 20);
            this.textBoxFrequency.TabIndex = 7;
            this.textBoxFrequency.TextChanged += new System.EventHandler(this.textBoxFrequency_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(113, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Частота (х раз день)";
            // 
            // maskedTextBoxFirstStart
            // 
            this.maskedTextBoxFirstStart.Location = new System.Drawing.Point(114, 44);
            this.maskedTextBoxFirstStart.Mask = "90:00";
            this.maskedTextBoxFirstStart.Name = "maskedTextBoxFirstStart";
            this.maskedTextBoxFirstStart.Size = new System.Drawing.Size(37, 20);
            this.maskedTextBoxFirstStart.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Первый запуск";
            // 
            // checkedListBoxDays
            // 
            this.checkedListBoxDays.FormattingEnabled = true;
            this.checkedListBoxDays.Items.AddRange(new object[] {
            "Понедельник",
            "Вторник",
            "Среда",
            "Четверг",
            "Пятница",
            "Суббота",
            "Воскресенье"});
            this.checkedListBoxDays.Location = new System.Drawing.Point(6, 44);
            this.checkedListBoxDays.Name = "checkedListBoxDays";
            this.checkedListBoxDays.Size = new System.Drawing.Size(94, 109);
            this.checkedListBoxDays.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Дни";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // textBoxMinPause
            // 
            this.textBoxMinPause.Location = new System.Drawing.Point(1255, 45);
            this.textBoxMinPause.Name = "textBoxMinPause";
            this.textBoxMinPause.Size = new System.Drawing.Size(37, 20);
            this.textBoxMinPause.TabIndex = 16;
            this.textBoxMinPause.Text = "1";
            this.textBoxMinPause.Leave += new System.EventHandler(this.textBoxMinPause_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1100, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Минимальная пауза (минут)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Location = new System.Drawing.Point(12, 495);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(719, 325);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sheduler";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.HorizontalScrollbar = true;
            this.listBox2.Location = new System.Drawing.Point(6, 15);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(701, 303);
            this.listBox2.TabIndex = 0;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 466);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(112, 23);
            this.button4.TabIndex = 18;
            this.button4.Text = "Создать плейлист";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.HorizontalScrollbar = true;
            this.listBox3.Location = new System.Drawing.Point(93, 97);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(190, 238);
            this.listBox3.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(93, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Фоновая";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(306, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Озвучка";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(94, 52);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 23);
            this.button5.TabIndex = 22;
            this.button5.Text = "Добавить файлы";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.75F);
            this.buttonTest.Location = new System.Drawing.Point(408, 51);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(96, 23);
            this.buttonTest.TabIndex = 23;
            this.buttonTest.Text = "Тест громкости";
            this.buttonTest.UseVisualStyleBackColor = true;
            this.buttonTest.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // timerResetSheduler
            // 
            this.timerResetSheduler.Enabled = true;
            this.timerResetSheduler.Interval = 900000;
            this.timerResetSheduler.Tick += new System.EventHandler(this.timerResetSheduler_Tick);
            // 
            // volumeSliderMain
            // 
            this.volumeSliderMain.Location = new System.Drawing.Point(754, 52);
            this.volumeSliderMain.Name = "volumeSliderMain";
            this.volumeSliderMain.Size = new System.Drawing.Size(345, 23);
            this.volumeSliderMain.TabIndex = 13;
            this.volumeSliderMain.VolumeChanged += new System.EventHandler(this.volumeSliderMain_VolumeChanged);
            // 
            // backgroundWorkerTcpClient
            // 
            this.backgroundWorkerTcpClient.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerTcpClient_DoWork);
            // 
            // labelConnect
            // 
            this.labelConnect.AutoSize = true;
            this.labelConnect.Location = new System.Drawing.Point(300, 29);
            this.labelConnect.Name = "labelConnect";
            this.labelConnect.Size = new System.Drawing.Size(69, 13);
            this.labelConnect.TabIndex = 24;
            this.labelConnect.Text = "labelConnect";
            // 
            // labelCurrentSong
            // 
            this.labelCurrentSong.AutoSize = true;
            this.labelCurrentSong.Location = new System.Drawing.Point(18, 396);
            this.labelCurrentSong.Name = "labelCurrentSong";
            this.labelCurrentSong.Size = new System.Drawing.Size(78, 13);
            this.labelCurrentSong.TabIndex = 25;
            this.labelCurrentSong.Text = "Текущий трек";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(205, 52);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(89, 23);
            this.button6.TabIndex = 26;
            this.button6.Text = "Удалить фон";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // buttonDEL
            // 
            this.buttonDEL.BackgroundImage = global::SamberiAudio.Properties.Resources.DEL;
            this.buttonDEL.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonDEL.Location = new System.Drawing.Point(721, 222);
            this.buttonDEL.Name = "buttonDEL";
            this.buttonDEL.Size = new System.Drawing.Size(24, 24);
            this.buttonDEL.TabIndex = 29;
            this.buttonDEL.UseVisualStyleBackColor = true;
            this.buttonDEL.Click += new System.EventHandler(this.buttonDEL_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::SamberiAudio.Properties.Resources.DOWN;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.Location = new System.Drawing.Point(721, 160);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(24, 57);
            this.button7.TabIndex = 28;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // buttomUP
            // 
            this.buttomUP.BackgroundImage = global::SamberiAudio.Properties.Resources.UP;
            this.buttomUP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttomUP.Location = new System.Drawing.Point(721, 96);
            this.buttomUP.Name = "buttomUP";
            this.buttomUP.Size = new System.Drawing.Size(24, 57);
            this.buttomUP.TabIndex = 27;
            this.buttomUP.UseVisualStyleBackColor = true;
            this.buttomUP.Click += new System.EventHandler(this.buttomUP_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.maskedTextSilentStart);
            this.groupBox3.Controls.Add(this.maskedTextSilentEnd);
            this.groupBox3.Controls.Add(this.checkBoxSilent);
            this.groupBox3.Location = new System.Drawing.Point(751, 495);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(157, 100);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ночная тишина";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(102, 44);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(38, 13);
            this.label14.TabIndex = 13;
            this.label14.Text = "Конец";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(15, 44);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(44, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Начало";
            // 
            // maskedTextSilentStart
            // 
            this.maskedTextSilentStart.Enabled = false;
            this.maskedTextSilentStart.Location = new System.Drawing.Point(18, 60);
            this.maskedTextSilentStart.Mask = "90:00";
            this.maskedTextSilentStart.Name = "maskedTextSilentStart";
            this.maskedTextSilentStart.Size = new System.Drawing.Size(37, 20);
            this.maskedTextSilentStart.TabIndex = 11;
            // 
            // maskedTextSilentEnd
            // 
            this.maskedTextSilentEnd.Enabled = false;
            this.maskedTextSilentEnd.Location = new System.Drawing.Point(103, 60);
            this.maskedTextSilentEnd.Mask = "90:00";
            this.maskedTextSilentEnd.Name = "maskedTextSilentEnd";
            this.maskedTextSilentEnd.Size = new System.Drawing.Size(37, 20);
            this.maskedTextSilentEnd.TabIndex = 10;
            // 
            // checkBoxSilent
            // 
            this.checkBoxSilent.AutoSize = true;
            this.checkBoxSilent.Location = new System.Drawing.Point(7, 20);
            this.checkBoxSilent.Name = "checkBoxSilent";
            this.checkBoxSilent.Size = new System.Drawing.Size(114, 17);
            this.checkBoxSilent.TabIndex = 0;
            this.checkBoxSilent.Text = "Включить тишину";
            this.checkBoxSilent.UseVisualStyleBackColor = true;
            this.checkBoxSilent.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // timerSilentMode
            // 
            this.timerSilentMode.Enabled = true;
            this.timerSilentMode.Interval = 1000;
            this.timerSilentMode.Tick += new System.EventHandler(this.timerSilentMode_Tick);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Enabled = true;
            this.timerUpdate.Interval = 60000;
            this.timerUpdate.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(954, 381);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 31;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1436, 873);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.buttonDEL);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.buttomUP);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.labelCurrentSong);
            this.Controls.Add(this.labelConnect);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.buttonSaveFile);
            this.Controls.Add(this.textBoxMinPause);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.volumeSliderMain);
            this.Controls.Add(this.buttonLoadConfig);
            this.Controls.Add(this.buttonSaveConfig);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBoxTime);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "SamberiAudio";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStripListBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxTime.ResumeLayout(false);
            this.groupBoxTime.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Timer timerRenewSongTrackBar;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel labelCurrentTime;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripLabel labelTotalTime;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelIndex;
        private System.Windows.Forms.Label labelLenght;
        private System.Windows.Forms.Label labelVolume;
        private System.Windows.Forms.Label labelPath;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxVolume;
        private System.Windows.Forms.Label labelLenghtFile;
        private System.Windows.Forms.Label labelPathFile;
        private System.Windows.Forms.TextBox textBoxIndex;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonSaveFile;
        private System.Windows.Forms.Button buttonSaveConfig;
        private System.Windows.Forms.Button buttonLoadConfig;
        private NAudio.Gui.VolumeSlider volumeSliderFile;
        private NAudio.Gui.VolumeSlider volumeSliderMain;
        private System.Windows.Forms.GroupBox groupBoxTime;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.CheckedListBox checkedListBoxDays;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxFirstStart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxFrequency;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxLastStart;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxMinPause;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolStripMenuItem оПрграммеToolStripMenuItem;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DateTimePicker dateTimePickerStart;
        private System.Windows.Forms.DateTimePicker dateTimePickerLast;
        private System.Windows.Forms.CheckBox checkBoxAllDays;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripListBox;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonTest;
        private System.ComponentModel.BackgroundWorker backgroundWorkerTcpClient;
        private System.Windows.Forms.Label labelConnect;
        private System.Windows.Forms.Label labelCurrentSong;
        private System.Windows.Forms.Timer timerResetSheduler;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button buttomUP;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button buttonDEL;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox maskedTextSilentStart;
        private System.Windows.Forms.MaskedTextBox maskedTextSilentEnd;
        private System.Windows.Forms.CheckBox checkBoxSilent;
        private System.Windows.Forms.Timer timerSilentMode;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.ToolStripMenuItem багтрекерToolStripMenuItem;
        private System.Windows.Forms.Button button8;
    }
}

