﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamberiAudio
{
    [Serializable]
    public class Song : IComparable
    {
        private string path;
        private Single volume;
        private string days;
        private DateTime firstTimeStart, lastTimeStart;
        private int frequency;
        private string name;
        private DateTime firstDayStart, lastDayStart;

        public int CompareTo(object obj)
        {
            Song songTmp = obj as Song;
            if (this < songTmp)
            {
                return -1;
            }
            if (this > songTmp)
                return 1;
            return 0;
        }

        public Song()
        { }

        public Song(string path, float volume, string days, DateTime firstStart, DateTime lastStart, int frequency, string name)
        {
            this.Path = path;
            this.Volume = volume;
            this.Days = days;
            this.FirstTimeStart = firstStart;
            this.LastTimeStart = lastStart;
            this.Frequency = frequency;
            this.Name = name;
        }

        /// <summary>
        /// Для создания класса Song в шедулере. Убрана частота и время последнего запуска.
        /// Каждый экземпляр класса проигрывается только 1 раз.
        /// </summary>
        /// <param name="days">Только один день выбирать!</param>
        public Song(string path, float volume, int days, DateTime firstStart, string name)
        {
            this.Frequency = 0;
            this.LastTimeStart = new DateTime(1, 1, 1, 1, 1, 1, 0);

            this.Path = path;
            this.Volume = volume;
            for (int i = 0; i < 7; i++)
            {
                if (days == i)
                    this.Days += "1";
                else
                    this.Days += "0";
            }
            this.FirstTimeStart = firstStart;
            this.Name = name;
        }

        public static bool operator >(Song s1, Song s2) // S1 > s2
        {
            switch (SravnenDay(s1, s2))
            {
                case 1:
                    return false;
                case 0:
                    return true;
                case 3:
                    if (s1.firstTimeStart < s2.firstTimeStart)
                        return false;
                    else
                        return true;
            }
            return false;

        }

        public static bool operator <(Song s1, Song s2) // s1 < s2
        {
            switch (SravnenDay(s1, s2))
            {
                case 1:
                    return true;
                case 0:
                    return false;
                case 3:
                    if (s1.firstTimeStart < s2.firstTimeStart)
                        return true;
                    else
                        return false;
            }
            return false;
        }

        private static int SravnenDay(Song s1, Song s2)
        {
            for (int i = 0; i < 7; i++)
            {
                if (Convert.ToInt16(s1.days[i].ToString()) > Convert.ToInt16(s2.days[i].ToString()))
                {
                    return 1;
                }
                else
                {
                    if (Convert.ToInt16(s1.days[i].ToString()) < Convert.ToInt16(s2.days[i].ToString()))
                        return 0;
                }
            }
            return 3;
        }

        public string Path { get { return path;} set { path = value; } }
        public float Volume { get {return volume; } set { volume = value; } }
        public string Days { get { return days; } set { days = value; } }
        public DateTime FirstTimeStart { get {return firstTimeStart; } set { firstTimeStart = value; } }
        public DateTime LastTimeStart { get { return lastTimeStart; } set { lastTimeStart = value; } }
        public int Frequency { get {return frequency; } set { frequency = value; } }
        public string Name { get {return name; } set { name = value; } }

        public DateTime FirstDayStart { get => firstDayStart; set => firstDayStart = value; }
        public DateTime LastDayStart { get => lastDayStart; set => lastDayStart = value; }
    }
}
