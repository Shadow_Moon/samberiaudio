﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;
using System.IO;
using System.Security.Cryptography;
//using SocketMessaging;
using AutoUpdaterDotNET;


namespace SamberiAudio
{
    public partial class Form1 : Form
    {
        bool connect = false;
        private const string CONFPATH = "./config.ini";
#if DEBUG
        private const string UPDATEPATH = @"C:\Users\Shadow_Moon\Source\Repos\RepoSamberi\SamberiAudio\SamberiAudio\bin\Debug\";
#else
        private const string UPDATEPATH = @"\\sdiv-007.samberi.com\SamberiAudio\";
#endif
        private const int MAXINDEX = 0;
        IWavePlayer waveOutDevice;
        AudioFileReader audioFileReader;
        IWavePlayer waveOutDeviceFon;
        AudioFileReader audioFileReaderFon;
        IWavePlayer waveOutDeviceTest;
        AudioFileReader audioFileReaderTest;
        Config config;
        Sheduler sheduler;
        bool firstSong;
        int playIndex;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        //private TcpClient client;
        private bool shedulerReset = false;
        private bool silentOn = false;

        private string ComputeMD5Checksum(string path)
        {
            using (FileStream fs = System.IO.File.OpenRead(path))
            {
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] fileData = new byte[fs.Length];
                fs.Read(fileData, 0, (int)fs.Length);
                byte[] checkSum = md5.ComputeHash(fileData);
                string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
                return result;
            }
        }

        public Form1()
        {
            InitializeComponent();
            config = new Config(MAXINDEX);
            buttonLoadConfig_Click(null, null);           
            sheduler = new Sheduler();
            playIndex = 0;
            firstSong = true;
            dateTimePickerStart.Value = DateTime.Today;
            dateTimePickerLast.Value = dateTimePickerLast.MaxDate;
            dateTimePickerStart.CustomFormat = "dd MMMM yyyy - ddd";
            dateTimePickerLast.CustomFormat = "dd MMMM yyyy - ddd";
            logger.Info("Запуск приложения");
            //client = null;
            backgroundWorker1.WorkerSupportsCancellation = true;
            //System.Diagnostics.Debugger.Launch();
            AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;
            if (config.NumberGiper == 0)
            {
                SelectGiper selectGiper = new SelectGiper();
                selectGiper.ShowDialog(this);
                config.NumberGiper = Convert.ToInt32(selectGiper.maskedTextBox1.Text);
                this.Text = "SamberiAudio" + " Самбери-" + config.NumberGiper;
            }
            else
            {
                button4_Click(null, null);
                button1_Click(null, null);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CloseWaveOut();
            buttonTest.Enabled = false;
            logger.Info("Запущен проигрыватель");
            silentOn = false;
            if (backgroundWorker1 != null)
            {
                backgroundWorker1.CancelAsync();
                Thread.Sleep(100);
                CloseWaveOut();
                backgroundWorker1.Dispose();
                backgroundWorker1 = null;
                backgroundWorker1 = new BackgroundWorker(); 
                backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(backgroundWorker1_DoWork);
                backgroundWorker1.WorkerSupportsCancellation = true;
            }
            backgroundWorker1.RunWorkerAsync();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            buttonTest.Enabled = true;
            backgroundWorker1.CancelAsync();
            Thread.Sleep(100);
            CloseWaveOut();
        }

        /// <summary>
        /// Закрывает все аудиопотоки. Необходимо вызывать перед вызовом другого потока.
        /// </summary>
        /// <remarks></remarks>
        private void CloseWaveOut()
        {
            if (waveOutDevice != null)
            {
                waveOutDevice.Stop();
            }
            if (audioFileReader != null)
            {
                audioFileReader.Dispose();
                audioFileReader = null;
            }
            if (waveOutDevice != null)
            {
                waveOutDevice.Dispose();
                waveOutDevice = null;
            }
            if (waveOutDeviceFon != null)
            {
                waveOutDeviceFon.Stop();
            }
            if (audioFileReaderFon != null)
            {
                audioFileReaderFon.Dispose();
                audioFileReaderFon = null;
            }
            if (waveOutDeviceFon != null)
            {
                waveOutDeviceFon.Dispose();
                waveOutDeviceFon = null;
            }
            if (waveOutDeviceTest != null)
            {
                waveOutDeviceTest.Stop();
            }
            if (audioFileReaderTest != null)
            {
                audioFileReaderTest.Dispose();
                audioFileReaderTest = null;
            }
            if (waveOutDeviceTest != null)
            {
                waveOutDeviceTest.Dispose();
                waveOutDeviceTest = null;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog;
            openFileDialog = new OpenFileDialog
            {
                Multiselect = false,
                Filter = "Audio files (*.wav, *mp3)|*.mp3;*.wav|All files (*.*)|*.*"
            };
            openFileDialog.ShowDialog();
            for (int i = 0; i < openFileDialog.FileNames.Length; i++)
            {
                listBox1.Items.Add(openFileDialog.FileNames[i]);
            }
            if (listBox1.Items.Count > 0)
            {
                listBox1.SetSelected(0, true);
            }
            listBox1_Click(null, null);
        }

        private void listBox1_Click(object sender, EventArgs e)
        {
            checkBoxAllDays.Checked = false;
            if (listBox1.SelectedIndex < 0)
            {
                return;
            }
            AudioFileReader audioFileReadertmp;
            audioFileReadertmp = new AudioFileReader(listBox1.SelectedItem.ToString());
            textBoxIndex.Text = listBox1.SelectedIndex.ToString();
            labelPathFile.Text = listBox1.SelectedItem.ToString();
            label11.Text = listBox1.SelectedItem.ToString().Split('\\')[labelPathFile.Text.Split('\\').Length - 1];
            if (config.Song.Length > listBox1.SelectedIndex)
            {
                if (dateTimePickerStart.MinDate < config.Song[listBox1.SelectedIndex].FirstDayStart)
                {
                    dateTimePickerStart.Value = config.Song[listBox1.SelectedIndex].FirstDayStart;
                    dateTimePickerLast.Value = config.Song[listBox1.SelectedIndex].LastDayStart;
                }
            }
            else
            {
                dateTimePickerStart.Value = DateTime.Today;
                dateTimePickerLast.Value = dateTimePickerLast.MaxDate;
            }
            try
            {
                volumeSliderFile.Volume = (float)config.Song[listBox1.SelectedIndex].Volume;
            }
            catch (NullReferenceException ex)
            {
                logger.Error(ex, "NullReferenceException в листбокс1клик");
                //   throw;
            }
            catch (IndexOutOfRangeException ex)
            {
                logger.Error(ex, "IndexOutOfRangeException в листбокс1клик");
                volumeSliderFile.Volume = 0;
            }

            labelLenghtFile.Text = String.Format("{0:00}:{1:00}", (int)audioFileReadertmp.TotalTime.TotalMinutes, audioFileReadertmp.TotalTime.Seconds);
            LoadCheckedDays(listBox1.SelectedIndex);
            LoadTime(listBox1.SelectedIndex);

            audioFileReadertmp.Dispose();
            audioFileReadertmp = null;
        }


        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (waveOutDevice != null)
            {
                audioFileReader.CurrentTime = TimeSpan.FromSeconds(audioFileReader.TotalTime.TotalSeconds * trackBar1.Value / 100.0);
            }
        }

        private void TimerRenewTrackBar_Tick(object sender, EventArgs e)
        {
            try
            {
                if (waveOutDevice != null && audioFileReader != null && waveOutDeviceFon != null)
                {
                    if (waveOutDeviceFon.PlaybackState == PlaybackState.Paused)
                    {
                        lock (waveOutDevice)
                        {
                            //logger.Debug("WaveOutDevice locked в TimerRenewTrackBar_tick");
                            TimeSpan currentTime = (waveOutDevice.PlaybackState == PlaybackState.Stopped) ? TimeSpan.Zero : audioFileReader.CurrentTime;
                            trackBar1.Value = Math.Min(trackBar1.Maximum, (int)(100 * currentTime.TotalSeconds / audioFileReader.TotalTime.TotalSeconds));
                            labelCurrentTime.Text = String.Format("{0:00}:{1:00}", (int)currentTime.TotalMinutes, currentTime.Seconds);
                            labelCurrentSong.Text = "Озвучка: " + audioFileReader.FileName.Split('\\')[audioFileReader.FileName.Split('\\').Length - 1];
                            labelTotalTime.Text = String.Format("{0:00}:{1:00}", (int)audioFileReader.TotalTime.TotalMinutes, audioFileReader.TotalTime.Seconds);
                            //logger.Debug("WaveOutDevice unlocked в TimerRenewTrackBar_tick");
                        }
                    }
                }
                else
                {                    
                    if (waveOutDeviceFon != null && audioFileReaderFon != null)
                    {
                        lock (waveOutDeviceFon)
                        {
                            //logger.Debug("WaveOutDeviceFon locked в TimerRenewTrackBar_tick");
                            //logger.Trace("Начало обновление информации о треке фона");
                            TimeSpan currentTime = (waveOutDeviceFon.PlaybackState == PlaybackState.Stopped) ? TimeSpan.Zero : audioFileReaderFon.CurrentTime;
                            trackBar1.Value = Math.Min(trackBar1.Maximum, (int)(100 * currentTime.TotalSeconds / audioFileReaderFon.TotalTime.TotalSeconds));
                            labelCurrentTime.Text = String.Format("{0:00}:{1:00}", (int)currentTime.TotalMinutes, currentTime.Seconds);
                            labelCurrentSong.Text = "Фоновая: " + audioFileReaderFon.FileName.Split('\\')[audioFileReaderFon.FileName.Split('\\').Length - 1];
                            labelTotalTime.Text = String.Format("{0:00}:{1:00}", (int)audioFileReaderFon.TotalTime.TotalMinutes, audioFileReaderFon.TotalTime.Seconds);
                            //logger.Trace("Конец обновления информации о треке фона");
                            //logger.Debug("WaveOutDeviceFon unlocked в TimerRenewTrackBar_tick");
                        }                    
                    }
                    else
                        trackBar1.Value = 0;                    
                }
            }
            catch (NullReferenceException)
            {
                logger.Debug("В TimerRenewTrackBar_tick NullReferenceException");
            }
            catch (ArgumentNullException)
            {
                logger.Debug("В TimerRenewTrackBar_tick ArgumentNullException");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundWorker1.CancelAsync();
            Thread.Sleep(100);
            CloseWaveOut();
            logger.Info("Приложение закрыто");
        }

        private DateTime saveDateTime(string str)
        {
            DateTime dt;
            string[] tmp = new string[2];
            tmp = str.Split(':');
            if (tmp[0] == " " || tmp[1] == "")
            {
                dt = new DateTime(1, 1, 1, 0, 0, 0);
                return dt;
            }
            dt = new DateTime(1, 1, 1, Convert.ToInt32(tmp[0]), Convert.ToInt32(tmp[1]), 0);
            return dt;
        }

        private void buttonSaveFile_Click(object sender, EventArgs e)
        {
            if (textBoxIndex.Text != null)
            {
                int ind = Convert.ToInt32(textBoxIndex.Text);
                if (ind >= config.Song.Length)
                {
                    config.ResizeIncrement();
                }
                config.Song[ind].Path = labelPathFile.Text;
                config.Song[ind].Name = labelPathFile.Text.Split('\\')[labelPathFile.Text.Split('\\').Length - 1];
                config.Song[ind].Volume = volumeSliderFile.Volume;
                config.Song[ind].Days = "";
                for (int i = 0; i < checkedListBoxDays.Items.Count; i++)
                {
                    config.Song[ind].Days += Convert.ToInt32(checkedListBoxDays.GetItemChecked(i)).ToString();
                }
                config.Song[ind].FirstTimeStart = saveDateTime(maskedTextBoxFirstStart.Text);
                config.Song[ind].LastTimeStart = saveDateTime(maskedTextBoxLastStart.Text);
                config.Song[ind].Frequency = Convert.ToInt32(textBoxFrequency.Text);
                config.Song[ind].FirstDayStart = dateTimePickerStart.Value;
                config.Song[ind].LastDayStart = dateTimePickerLast.Value;
            }
        }

        private void buttonSaveConfig_Click(object sender, EventArgs e)
        {
            config.MinPause = Convert.ToInt32(textBoxMinPause.Text);
            config.SilentCheckBox = checkBoxSilent.Checked;
            try
            {
                config.SilentStart = saveDateTime(maskedTextSilentStart.Text);
                config.SilentEnd = saveDateTime(maskedTextSilentEnd.Text);
                if (config.SilentStart.Hour == config.SilentEnd.Hour)
                {
                    MessageBox.Show("Начало и конец тишины должны быть в разных часах");
                    return; 
                }
            }
            catch (System.ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Исправьте значения во времени начала и конца тишины, там не дата");
                return;
            }

            //System.Runtime.Serialization.Json.DataContractJsonSerializer jsonSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof (Config));
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Config));

            using (var fStream = new FileStream(CONFPATH, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlSerializer.Serialize(fStream, config);
            }

            config.SaveSQL();
        }

        private void buttonLoadConfig_Click(object sender, EventArgs e)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Config));

            try
            {
                using (var fStream = new FileStream(CONFPATH, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    try
                    {
                        config = (Config)xmlSerializer.Deserialize(fStream);
                    }
                    catch (InvalidOperationException)
                    {
                        logger.Info("Файл config.ini неисправен");
                        return;
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                logger.Info("Файла config.ini не найдено");
                return;
            }

            if (config.NumberGiper == 0)
            {
                SelectGiper selectGiper = new SelectGiper();
                selectGiper.ShowDialog(this);
                config.NumberGiper = Convert.ToInt32(selectGiper.maskedTextBox1.Text);
            }
            this.Text = "SamberiAudio" + " Самбери-" + config.NumberGiper;

            listBox1.Items.Clear();
            listBox3.Items.Clear();
            if (config.Song != null)
                for (int i = 0; i < config.Song.Length; i++)
                {
                    if (config.Song[i].Path != null)
                    {
                        listBox1.Items.Add(config.Song[i].Path);
                    }

                }
            if (config.SongFON != null)
                for (int i = 0; i < config.SongFON.Length; i++)
                {
                    if (config.SongFON[i].Path != null)
                    {
                        listBox3.Items.Add(config.SongFON[i].Path);
                    }
                }
            volumeSliderMain.Volume = config.MainVolume;
            if (listBox1.Items.Count > 0)
            {
                listBox1.SetSelected(0, true);
                listBox1_Click(null, null);
            }
            textBoxMinPause.Text = config.MinPause.ToString();

            checkBoxSilent.Checked = config.SilentCheckBox;
            maskedTextSilentStart.Text = LoadDateTime(config.SilentStart);
            maskedTextSilentEnd.Text = LoadDateTime(config.SilentEnd);
            
        }

        private string LoadDateTime(DateTime dt)
        {
            string str;
            if (dt.Hour <= 9)
            {
                str = "0" + dt.Hour.ToString();
            }
            else
            {
                str = dt.Hour.ToString();
            }
            if (dt.Minute <= 9)
            {
                str += ":" + "0" + dt.Minute.ToString();
            }
            else
            {
                str += ":" + dt.Minute.ToString();
            }
            return str;
        }

        private void LoadTime(int index)
        {
            try
            {
                maskedTextBoxFirstStart.Text = LoadDateTime(config.Song[index].FirstTimeStart);
                maskedTextBoxLastStart.Text = LoadDateTime(config.Song[index].LastTimeStart);
                textBoxFrequency.Text = config.Song[index].Frequency.ToString();
            }
            catch (NullReferenceException)
            {

            }
            catch (IndexOutOfRangeException)
            {
                maskedTextBoxFirstStart.Text = "";
                maskedTextBoxLastStart.Text = "";
                textBoxFrequency.Text = "";
            }

        }

        private void LoadCheckedDays(int index)
        {
            try
            {
                for (int j = 0; j < checkedListBoxDays.Items.Count; j++)
                {
                    checkedListBoxDays.SetItemChecked(j, Convert.ToBoolean(Convert.ToInt16(config.Song[index].Days[j].ToString())));
                }
            }
            catch (NullReferenceException)
            {

            }
            catch (IndexOutOfRangeException)
            {
                for (int j = 0; j < checkedListBoxDays.Items.Count; j++)
                {
                    checkedListBoxDays.SetItemChecked(j, false);
                }
            }

        }

        private void volumeSliderMain_VolumeChanged(object sender, EventArgs e)
        {
            if (audioFileReader != null)
            { audioFileReader.Volume = volumeSliderMain.Volume * volumeSliderFile.Volume; }
            if (audioFileReaderFon != null)
            { audioFileReaderFon.Volume = volumeSliderMain.Volume; }
            if (audioFileReaderTest != null)
            { audioFileReaderTest.Volume = volumeSliderMain.Volume * volumeSliderFile.Volume; }
            config.MainVolume = volumeSliderMain.Volume;
        }

        private void volumeSliderFile_VolumeChanged(object sender, EventArgs e)
        {
            /*if (audioFileReader != null)
            { audioFileReader.Volume = volumeSliderMain.Volume * volumeSliderFile.Volume; }*/
            if (audioFileReaderTest != null)
            { audioFileReaderTest.Volume = volumeSliderMain.Volume * volumeSliderFile.Volume; }
            textBoxVolume.Text = textBoxVolumeWriter();
        }

        private string textBoxVolumeWriter()
        {
            return Convert.ToString(20 * (float)Math.Log10((float)volumeSliderFile.Volume));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timerUpdate_Tick(null, null);
            backgroundWorkerTcpClient.RunWorkerAsync();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CheckLastDay();
            sheduler.DoSheduler(config);
            sheduler.DoShedulerFon(config);
            listBox2.Items.Clear();
            listBox2.Items.AddRange(sheduler.ArrayList2ListBox().Items);
        }

        private void ограниченияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SamberiAudio.Разные_окна.Ограничения frm = new Разные_окна.Ограничения();
            frm.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog;
            openFileDialog = new OpenFileDialog
            {
                Multiselect = true,
                Filter = "Audio files (*.wav, *mp3)|*.mp3;*.wav|All files (*.*)|*.*"
            };
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                for (int i = 0; i < openFileDialog.FileNames.Length; i++)
                {
                    listBox3.Items.Add(openFileDialog.FileNames[i]);
                }
                if (listBox3.Items.Count > 0)
                {
                    listBox3.SetSelected(0, true);
                }

                config.ResizeIncrementАFON(openFileDialog.FileNames.Length);

                for (int i = 0; i < listBox3.Items.Count; i++)
                {
                    if (config.SongFON[i] == null)
                    {
                        config.SongFON[i] = new Song("", 0, 0, DateTime.Now, "");
                    }
                    config.SongFON[i].Path = listBox3.Items[i].ToString();
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            bool tmp = false;
            if (config.Song[0] < config.Song[1])
                tmp = true;
            MessageBox.Show(tmp.ToString());
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker thisWorker = (BackgroundWorker)sender;
            if ((playIndex = FindFirstSong()) != -1)
            {
                logger.Info("Первый файл для запуска будет: {0} в {1} в день недели {2}", ((Song)sheduler.ShedulerArray[playIndex]).Name,
                    ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart, Utils.FindDayOfWeek(((Song)sheduler.ShedulerArray[playIndex])).ToString());
            }
            PlayAllBack(thisWorker);
        }

        private void PlayAllBack(BackgroundWorker thisWorker)
        {
            Song tmpFon;
            Song tmp;
            int count = 0;

            CloseWaveOut();

            tmpFon = sheduler.ShedulerFon.Dequeue();
            sheduler.ShedulerFon.Enqueue(tmpFon);
            audioFileReaderFon = new AudioFileReader(tmpFon.Path);
            waveOutDeviceFon = new WaveOut();
            audioFileReaderFon.Volume = volumeSliderMain.Volume;
            waveOutDeviceFon.Init(audioFileReaderFon);
            logger.Debug("Проигрывается фоновый трек {0}", audioFileReaderFon.FileName);
            waveOutDeviceFon.Play();

            while (thisWorker.CancellationPending == false)
            {
                if (audioFileReaderFon.CurrentTime.TotalSeconds > audioFileReaderFon.TotalTime.TotalSeconds - 1)
                {
                    tmpFon = sheduler.ShedulerFon.Dequeue();
                    sheduler.ShedulerFon.Enqueue(tmpFon);
                    if (audioFileReaderFon != null)
                    {
                        audioFileReaderFon.Dispose();
                        audioFileReaderFon = null;
                    }
                    audioFileReaderFon = new AudioFileReader(tmpFon.Path);
                    audioFileReaderFon.Volume = volumeSliderMain.Volume;
                    logger.Debug("Проигрывается фоновый трек {0}", audioFileReaderFon.FileName);
                    if (waveOutDeviceFon != null)
                    {
                        waveOutDeviceFon.Dispose();
                    }
                    waveOutDeviceFon.Init(audioFileReaderFon);
                    waveOutDeviceFon.Play();
                }
                //count++;
                Thread.Sleep(100);
                //logger.Debug("В проигрывателе прошла пауза 2 сек");
                while (PlaySheduler(out tmp))
                {
                    if (waveOutDeviceFon != null)
                        lock (waveOutDeviceFon)
                        {
                            logger.Debug("Пытаемся поставить на паузу waveOutDeviceFon");
                            waveOutDeviceFon.Pause();
                            logger.Debug("Поставлен на паузу waveOutDeviceFon");
                        }
                    Thread.Sleep(500);
                    if (waveOutDevice != null)
                    {
                        waveOutDevice.Dispose();
                        waveOutDevice = null;
                    }
                    waveOutDevice = new WaveOut();
                    if (audioFileReader != null)
                    {
                        audioFileReader.Dispose();
                        audioFileReader = null;
                    }

                    audioFileReader = new AudioFileReader(tmp.Path);
                    //logger.Info("Запущен трек {0}", tmp.Path);
                    waveOutDevice.Init(audioFileReader);
                    lock (waveOutDevice)
                    {
                        audioFileReader.Volume = tmp.Volume * volumeSliderMain.Volume;
                        waveOutDevice.Play();
                        //logger.Debug("Обновляем SQL");
                        PlayNowSQL(audioFileReader);
                        logger.Debug("Будет слип на {0} секунд", (int)audioFileReader.TotalTime.TotalSeconds + 2);
                    }
                    Thread.Sleep((int)audioFileReader.TotalTime.TotalSeconds * 1000 + 2000);
                    PlayNowEndSQL(audioFileReader);
                    audioFileReader.Dispose();
                    audioFileReader = null;
                    logger.Debug("Слип прошел");
                    waveOutDeviceFon.Play();
                }
            }
        }

        private void PlayNowEndSQL(AudioFileReader audioFile)
        {
            try
            {
                using (var db = new SQL.postgresContext())
                {
                    var update = db.PlayedSongs.Where(s => s.IdSongNavigation.Song == audioFile.FileName.Split('\\')[audioFile.FileName.Split('\\').Length - 1] && s.Gm == config.NumberGiper).SingleOrDefault();
                    update.NowPlayed = false;
                    db.SaveChanges();
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                logger.Info(ex, "Нет подключения к серверу SQL, функция PlayNowEndSQL");
            }
            catch (System.InvalidOperationException ex)
            {
                logger.Info(ex, "Нет подключения к серверу SQL, функция PlayNowEndSQL");
            }
        }

        private void PlayNowSQL(AudioFileReader audioFile)
        {
            try
            {
                using (var db = new SQL.postgresContext())
                {
                    if (db.PlayedSongs.Where(s => s.IdSongNavigation.Song == audioFile.FileName.Split('\\')[audioFile.FileName.Split('\\').Length - 1] && s.Gm == config.NumberGiper).Count() == 0)
                    {
                        db.PlayedSongs.Add(AddPlayedSongSQL(audioFile, db.Songs.Where(s => s.Song == audioFile.FileName.Split('\\')[audioFile.FileName.Split('\\').Length - 1] && s.Gm == config.NumberGiper).SingleOrDefault().Id));
                    }
                    else
                    {
                        var update = db.PlayedSongs.Where(s => s.IdSongNavigation.Song == audioFile.FileName.Split('\\')[audioFile.FileName.Split('\\').Length - 1] && s.Gm == config.NumberGiper).SingleOrDefault();
                        update.LastTime = DateTime.Now;
                        update.NowPlayed = true;
                    }
                    db.SaveChanges();
                }
            }
            catch(System.Net.Sockets.SocketException ex)
            {
                logger.Info(ex.Message, "Нет подключения к серверу SQL, функция PlayNowSQL");
            }
            catch(System.InvalidOperationException ex)
            {
                logger.Info(ex.Message, "Нет подключения к серверу SQL, функция PlayNowSQL");
            }
        }

        private SQL.PlayedSongs AddPlayedSongSQL (AudioFileReader audioFile, int idSong)
        {
            var tmp = new SQL.PlayedSongs();
            tmp.NowPlayed = true;
            tmp.LastTime = DateTime.Now;
            tmp.Gm = config.NumberGiper;
            tmp.IdSong = idSong;
            return tmp;
        }

        /// <returns>
        /// Возвращает индекс песни которая должна играть первая после запуска приложения. Вызывается на старте только 1 раз.
        /// </returns>
        private int FindFirstSong()
        {
            Song songNow = new Song("", 0, 0, DateTime.Now.AddSeconds(1), "");
            Song song;
            int tmp = 0;
            if (sheduler.ShedulerArray.Count == 1)
                return 0;
            if (sheduler.ShedulerArray.Count == 0)
            {
                logger.Debug("PlayIndex == -1, нечего проигрывать играет только фон");
                return -1;
            }
            song = ((Song)sheduler.ShedulerArray[0]);            
            for (int i = 0; i < sheduler.ShedulerArray.Count; i++)
            {
                if (sheduler.ShedulerArray.Count == (i + 1))
                    continue;
                if (((Song)sheduler.ShedulerArray[i]).FirstTimeStart.DayOfWeek == DateTime.Now.DayOfWeek)
                    if (((Song)sheduler.ShedulerArray[i]).FirstTimeStart < songNow.FirstTimeStart)
                    {
                        tmp = i;
                    }
                else
                    {
                        tmp = i;
                        return tmp;
                    }
            }
            return ++tmp;
        }

        private bool PlaySheduler(out Song song)
        {
            song = new Song("", 0, 0, DateTime.Now, "");
            if (playIndex == -1)
            {
                return false;
            }            
            //logger.Debug("Переменная firstSong {0}", firstSong);
            if (firstSong)
            {
                // logger.Debug("Сравнение дня недели: {0}", Utils.FindDayOfWeek(((Song)sheduler.ShedulerArray[playIndex])) == DateTime.Now.DayOfWeek);
                if (Utils.FindDayOfWeek(((Song)sheduler.ShedulerArray[playIndex])) == DateTime.Now.DayOfWeek)
                {
                    //  logger.Debug("Сравнение часа: {0}", DateTime.Now.Hour == ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart.Hour);
                    if (DateTime.Now.Hour == ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart.Hour)
                    {
                        // logger.Debug("Сравнение минут: {0}", DateTime.Now.Minute == ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart.Minute);
                        if (DateTime.Now.Minute == ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart.Minute)
                            if (DateTime.Now.Second == ((Song)sheduler.ShedulerArray[playIndex]).FirstTimeStart.Second)
                            {
                                song = ((Song)sheduler.ShedulerArray[playIndex]);
                                logger.Info("Запущен первый трек {0}", song.Path);
                                try
                                {
                                    logger.Info("Следующий трек {0} в {1}", ((Song)sheduler.ShedulerArray[playIndex + 1]).Path, ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart);
                                }
                                catch
                                {
                                    logger.Info("Сегодня больше нечего проигрывать, спасибо за прослушивание");
                                }
                                firstSong = false;
                                return true;
                            }
                    }
                }
            }
            if (sheduler.ShedulerArray.Count == (playIndex + 1))
            {
                if (Utils.FindDayOfWeek(((Song)sheduler.ShedulerArray[0])) == DateTime.Now.DayOfWeek)
                    if (DateTime.Now.Hour == ((Song)sheduler.ShedulerArray[0]).FirstTimeStart.Hour)
                        if (DateTime.Now.Minute == ((Song)sheduler.ShedulerArray[0]).FirstTimeStart.Minute)
                            if (DateTime.Now.Second == ((Song)sheduler.ShedulerArray[0]).FirstTimeStart.Second)
                            {
                                playIndex = 0;
                                song = ((Song)sheduler.ShedulerArray[playIndex]);
                                logger.Info("Запущен трек {0}", song.Path);
                                try
                                {
                                    logger.Info("Следующий трек {0} в {1}", ((Song)sheduler.ShedulerArray[playIndex + 1]).Path, ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart);
                                }
                                catch
                                {
                                    logger.Info("Сегодня больше нечего проигрывать, спасибо за прослушивание");
                                }
                                return true;
                            }
            }
            else
            {
                if (Utils.FindDayOfWeek(((Song)sheduler.ShedulerArray[playIndex + 1])) == DateTime.Now.DayOfWeek)
                    if (DateTime.Now.Hour == ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart.Hour)
                        if (DateTime.Now.Minute == ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart.Minute)
                            if (DateTime.Now.Second == ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart.Second)
                            {
                                playIndex++;
                                song = ((Song)sheduler.ShedulerArray[playIndex]);
                                logger.Info("Запущен трек {0}", song.Path);
                                try
                                {
                                    logger.Info("Следующий трек {0} в {1}", ((Song)sheduler.ShedulerArray[playIndex + 1]).Path, ((Song)sheduler.ShedulerArray[playIndex + 1]).FirstTimeStart);
                                }
                                catch
                                {
                                    logger.Info("Сегодня больше нечего проигрывать, спасибо за прослушивание");
                                }
                                return true;
                            }
            }
            return false;
        }

        private void checkBoxAllDays_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {
                for (int i = 0; i < checkedListBoxDays.Items.Count; i++)
                {
                    checkedListBoxDays.SetItemChecked(i, true);
                }
            }
            else
            {
                for (int i = 0; i < checkedListBoxDays.Items.Count; i++)
                {
                    checkedListBoxDays.SetItemChecked(i, false);
                }
            }
        }

        private void listBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                int index = this.listBox1.IndexFromPoint(e.Location);
                if (index != ListBox.NoMatches)
                {
                    listBox1.SelectedIndex = index;
                }
            }
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null || listBox1.SelectedIndex < 0)
                return;
            var index = listBox1.SelectedIndex;
            config.DeleteSong(listBox1.SelectedIndex);
            listBox1.Items.Remove(listBox1.Items[listBox1.SelectedIndex]);
            if (listBox1.SelectedIndex < 0)
            {
                if ((index - 1) >= 0)
                    listBox1.SelectedIndex = index - 1;
                else
                {
                    if (listBox1.Items.Count != 0)
                        listBox1.SelectedIndex = index;
                }
            }
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            if (waveOutDeviceTest != null)
                if (waveOutDeviceTest.PlaybackState == PlaybackState.Playing)
                { CloseWaveOut(); return; }
            CloseWaveOut();
            audioFileReaderTest = new AudioFileReader((string)listBox1.SelectedItem);
            waveOutDeviceTest = new WaveOut();
            audioFileReaderTest.Volume = volumeSliderMain.Volume * volumeSliderFile.Volume;
            waveOutDeviceTest.Init(audioFileReaderTest);
            waveOutDeviceTest.Play();
        }

        private void timerResetSheduler_Tick(object sender, EventArgs e)
        {
            if (shedulerReset == false && DateTime.Now.Hour == 3)
            {
                logger.Debug("Проверяем есть ли треки для удаления");
                CheckLastDay();
                logger.Debug("Начало резета шедулера, останавливаем плеер");
                button2_Click(Type.Missing, e);
                Thread.Sleep(100000);
                logger.Debug("Прошел слип 100 сек в резете шедулера, приступаю к резету шедулера");
                button4_Click(Type.Missing, e);
                Thread.Sleep(600000);
                logger.Debug("Прошел слип в 10 минут в резете шедулера, шедулер обновлен");
                if (checkBoxSilent.Checked == false)
                {
                    button1_Click(Type.Missing, e);
                    logger.Debug("Запуск озвучки");
                }
                shedulerReset = true;
            }
            if (DateTime.Now.Hour == 4 && shedulerReset == true)
            {
                shedulerReset = false;
                logger.Debug("shedulerReset установлен в false");
            }
        }

        /// <summary>
        /// Проверяет, нужно ли в текущем дне проигрывать треки озвучки, если находит трек который не должен больше играть, то удаляет его и пересохраняет конфиг.
        /// </summary>
        private void CheckLastDay()
        {
            for (int i = 0; i < config.Song.Length; i++)
            {
                if (config.Song[i].LastDayStart.AddDays(1) < DateTime.Now)
                {
                    logger.Info("Будет удален файл {0}, который больше не будет проигрываться", config.Song[i].Name);
                    config.DeleteSong(i);
                    listBox1.Items.Remove(listBox1.Items[i]);
                    logger.Info("Успешно удалили файл, сохраняем конфиг");
                    buttonSaveConfig_Click(Type.Missing, null);
                }
            }
        }

        private void backgroundWorkerTcpClient_DoWork(object sender, DoWorkEventArgs e)
        {
            var alive = false;
            while (true)
            {
                try
                {
                    using (var db = new SQL.postgresContext())
                    {
                        SQL.Connected gm = new SQL.Connected();
                        try
                        {
                            gm = db.Connected.Where(s => s.Name == config.NumberGiper).SingleOrDefault();
                            alive = true;
                        }
                        catch (System.Net.Sockets.SocketException ex)
                        {
                            alive = false;
                        }
                        if (alive)
                        {
                            if (gm != null)
                            {
                                gm.Time = DateTime.Now;
                                gm.IsConnected = true;
                                gm.Version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                            }
                            else
                            {
                                var newGM = new SQL.Connected();
                                newGM.IsConnected = true;
                                newGM.Time = DateTime.Now;
                                newGM.Name = config.NumberGiper;
                                db.Connected.Add(newGM);
                            }
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex/*System.InvalidOperationException*/)
                {
                    logger.Debug(ex, "System.InvalidOperationException в backgroundWorkerTcpClient_DoWork. Скорее всего нет связи с SQL.");
                }
                if (alive)
                {
                    this.labelConnect.BeginInvoke((MethodInvoker)(() => this.labelConnect.Text = "Подключен к серверу"));
                    this.labelConnect.BeginInvoke((MethodInvoker)(() => this.labelConnect.BackColor = Color.Green));                    
                }
                else
                {
                    this.labelConnect.BeginInvoke((MethodInvoker)(() => this.labelConnect.Text = "Не подключен к серверу"));
                    this.labelConnect.BeginInvoke((MethodInvoker)(() => this.labelConnect.BackColor = Color.Red));
                }
                System.Threading.Thread.Sleep(10000);
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < config.SongFON.Length; i++)
            {
                config.SongFON[i] = null;
            }
            config.SongFON = null;
            listBox3.Items.Clear();
        }

        private void buttomUP_Click(object sender, EventArgs e)
        {
            MoveItem(-1);
        }

        private void MoveItem(int direction)
        {
            // Checking selected item
            if (listBox1.SelectedItem == null || listBox1.SelectedIndex < 0)
                return; // No selected item - nothing to do

            // Calculate new index using move direction
            int newIndex = listBox1.SelectedIndex + direction;

            // Checking bounds of the range
            if (newIndex < 0 || newIndex >= listBox1.Items.Count)
                return; // Index out of range - nothing to do

            object selected = listBox1.SelectedItem;
            var song = new Song();
            song = config.Song[listBox1.SelectedIndex + direction];
            config.Song[listBox1.SelectedIndex + direction] = config.Song[listBox1.SelectedIndex];
            config.Song[listBox1.SelectedIndex] = song;
            // Removing removable element
            listBox1.Items.Remove(selected);
            // Insert it in new position
            listBox1.Items.Insert(newIndex, selected);
            // Restore selection
            listBox1.SetSelected(newIndex, true);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MoveItem(1);
        }

        private void buttonDEL_Click(object sender, EventArgs e)
        {
            удалитьToolStripMenuItem_Click(sender, e);
        }

        private void textBoxFrequency_TextChanged(object sender, EventArgs e)
        {
            DateTime dtStart, dtLast;
            dtStart = saveDateTime(maskedTextBoxFirstStart.Text);
            dtLast = saveDateTime(maskedTextBoxLastStart.Text);
            switch (textBoxFrequency.Text)
            {
                case "1":
                    {
                        label12.Text = string.Format("Будет проигрываться только в {0}:{1}",
                            dtStart.Hour, dtStart.Minute);
                        break;
                    }
                case "2":
                    {
                        label12.Text = string.Format("Будет проигрываться только в {0}:{1} \r\n" +
                                                     "                                               и в {2}:{3}",
                            dtStart.Hour, dtStart.Minute,
                            dtLast.Hour, dtLast.Minute);
                        break;
                    }
                default:
                    {
                        if (textBoxFrequency.Text != "" && maskedTextBoxFirstStart.Text != ":" && maskedTextBoxLastStart.Text != ":")
                        {
                            label12.Text = string.Format("Будет проигрываться раз в {0} минут",
                                (dtLast.Hour * 60 + dtLast.Minute - dtStart.Hour * 60 + dtStart.Minute) / Convert.ToInt32(textBoxFrequency.Text));
                        }
                        break;
                    }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxSilent.Checked)
            {
                maskedTextSilentStart.Enabled = true;
                maskedTextSilentEnd.Enabled = true;
            }
            else
            {
                maskedTextSilentStart.Enabled = false;
                maskedTextSilentEnd.Enabled = false;
            }
        }

        private void timerSilentMode_Tick(object sender, EventArgs e)
        {
            //logger.Trace("СайлентМодТик начало");
            if (DateTime.Now.Hour == config.SilentStart.Hour &&
                 (DateTime.Now.Minute == config.SilentStart.Minute) &&
                     (!silentOn) &&
                    checkBoxSilent.Checked == true)
            {
                silentOn = true;
                logger.Info("Отключаем проигрыватель ночью");
                button2_Click(Type.Missing, e);
            }
            else
            if (DateTime.Now.Hour == config.SilentEnd.Hour)
                if (DateTime.Now.Minute == config.SilentEnd.Minute)
                    if (silentOn)
                    {
                        silentOn = false;
                        logger.Info("Включение проигрывателя после ночи");
                        button1_Click(Type.Missing, e);
                    }
        }

        private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
        {
            if (args != null /*&& DateTime.Now.Hour == 2*/)
            {
                if (args.IsUpdateAvailable)
                {
                    try
                    {
                        if (AutoUpdater.DownloadUpdate())
                        {
                            Application.Exit();
                        }
                    }
                    catch (Exception exception)
                    {
                        logger.Info(exception.Message, exception.GetType().ToString());
                    }
                }
            }
           
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            try
            {
                AutoUpdater.RunUpdateAsAdmin = false;
                AutoUpdater.Start(UPDATEPATH + "Version.xml");
            }
            catch
            {
                logger.Debug("В timerUpdate_Tick эксепшн. Не доступен сервер обновления");
            }



            /*Version lattestVersion;
            var currentVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            try
            {
                using (StreamReader sr = new StreamReader(File.Open(UPDATEPATH + @"Version.txt", FileMode.Open)))
                {
                    lattestVersion = new Version(sr.ReadLine());
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                logger.Info("Нет файла Version.txt по пути {1}", UPDATEPATH + @"Version.txt");
                return;
            }
            if (
                //DateTime.Now.Hour == 2
                true 
                && currentVersion < lattestVersion
                )
            {
                CopyDir(UPDATEPATH, @".\Update\");
            }*/
        }

        private void textBoxMinPause_Leave(object sender, EventArgs e)
        {
            try
            {
                Convert.ToInt32(this.textBoxMinPause.Text);
            }
            catch (Exception)
            {                
                MessageBox.Show("Минимальная пауза только целое положительное число","Ошибка",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (Convert.ToDouble(textBoxMinPause.Text) < 0)
                {
                    MessageBox.Show("Минимальная пауза только целое положительное число", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                textBoxMinPause.Text = "1";
            }
        }

        private void багтрекерToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/Shadow_Moon/samberiaudio/issues");
        }

        private void оПрграммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new SamberiAudio.Разные_окна.About();
            frm.ShowDialog();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var url = "http://radiocast.muzlab.ru/jjsr.aac";
            var mf = new MediaFoundationReader(url);
            using (var wo = new WaveOutEvent())
            {
                wo.Init(mf);
                wo.Play();
                while (wo.PlaybackState == PlaybackState.Playing)
                {
                    Thread.Sleep(1000);

                    while ((wo.PlaybackState == PlaybackState.Stopped) || (wo.PlaybackState == PlaybackState.Paused))
                    {
                        System.Windows.Forms.MessageBox.Show("Stop");
                        try
                        {
                            mf = new MediaFoundationReader(url);
                            wo.Init(mf);
                            wo.Play();
                        }
                        catch { }
                        Thread.Sleep(2000);
                    }
                }
            }
        }
    }    
}