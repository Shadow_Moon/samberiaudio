﻿using System;
using System.Collections;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamberiAudio
{
    class Sheduler
    {

        private ArrayList shedulerArray;
        private Queue<Song> shedulerFon;

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();


        public Sheduler()
        {
            shedulerArray = new ArrayList();
            shedulerFon = new Queue<Song>();
        }

        public System.Windows.Forms.ListBox ArrayList2ListBox()
        {
            System.Windows.Forms.ListBox tmpListBox = new System.Windows.Forms.ListBox();
            for (int i = 0; i < shedulerArray.Count; i++)
            {
                tmpListBox.Items.Add(string.Format("{0} {1} {2}", ((Song)shedulerArray[i]).Name, ((Song)shedulerArray[i]).FirstTimeStart, Utils.FindDayOfWeek(((Song)shedulerArray[i])).ToString()));
            }
            return tmpListBox;
        }

        public void DoSheduler(Config conf)
        {
            double minutes_Between_firstAndLastTime = 0;
            shedulerArray.Clear();
            for (int i = 0; i < conf.Song.Length; i++)
            {
                if (conf.Song[i].FirstDayStart < DateTime.Now)
                {
                    for (int j = 0; j < conf.Song[i].Days.Length; j++)
                    {
                        if (Utils.FindDayOfWeek(conf.Song[i], DateTime.Now, j))
                        {
                            if (conf.Song[i].Frequency == 1)
                            {
                                shedulerArray.Add(new Song(
                                conf.Song[i].Path,
                                conf.Song[i].Volume,
                                j,
                                conf.Song[i].FirstTimeStart,
                                conf.Song[i].Name
                                ));
                            }
                            else if (conf.Song[i].Frequency == 2)
                            {
                                shedulerArray.Add(new Song(
                                conf.Song[i].Path,
                                conf.Song[i].Volume,
                                j,
                                conf.Song[i].FirstTimeStart,
                                conf.Song[i].Name
                                ));
                                shedulerArray.Add(new Song(
                                conf.Song[i].Path,
                                conf.Song[i].Volume,
                                j,
                                conf.Song[i].LastTimeStart,
                                conf.Song[i].Name
                                ));
                            }
                            else
                            {
                                shedulerArray.Add(new Song(
                                conf.Song[i].Path,
                                conf.Song[i].Volume,
                                j,
                                conf.Song[i].FirstTimeStart,
                                conf.Song[i].Name
                                ));
                                shedulerArray.Add(new Song(
                                conf.Song[i].Path,
                                conf.Song[i].Volume,
                                j,
                                conf.Song[i].LastTimeStart,
                                conf.Song[i].Name
                                ));
                                minutes_Between_firstAndLastTime =
                                    ((conf.Song[i].LastTimeStart.Hour * 60 + conf.Song[i].LastTimeStart.Minute) -
                                    (conf.Song[i].FirstTimeStart.Hour * 60 + conf.Song[i].FirstTimeStart.Minute)) /
                                    (conf.Song[i].Frequency - 1);
                                for (int k = 1; k < conf.Song[i].Frequency - 1; k++)
                                {
                                    shedulerArray.Add(new Song(
                                    conf.Song[i].Path,
                                    conf.Song[i].Volume,
                                    j,
                                    conf.Song[i].FirstTimeStart.AddMinutes(minutes_Between_firstAndLastTime * k),
                                    conf.Song[i].Name
                                    ));
                                }
                            }
                        }
                    }
                }
            }
            shedulerArray.Sort();
            SetDate();
            CheckCollisions(conf.MinPause);
            shedulerArray.Sort();
        }

        private void SetDate()
        {
            var dt = Utils.FindMonday();
            for (int i = 0; i < shedulerArray.Count; i++)
            {
                switch (Utils.FindDayOfWeek((Song)shedulerArray[i]))
                {
                    case DayOfWeek.Monday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 0);
                        break;
                    case DayOfWeek.Tuesday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 1);
                        break;
                    case DayOfWeek.Wednesday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 2);
                        break;
                    case DayOfWeek.Thursday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 3);
                        break;
                    case DayOfWeek.Friday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 4);
                        break;
                    case DayOfWeek.Saturday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 5);
                        break;
                    case DayOfWeek.Sunday:
                        ((Song)shedulerArray[i]).FirstTimeStart = SetDateTime(dt, ((Song)shedulerArray[i]).FirstTimeStart, 6);
                        break;

                    default:
                        break;
                }
            }
        }

        private DateTime SetDateTime(DateTime dt, DateTime dtSong , int i)
        {
            dtSong = dtSong.AddYears(dt.Year - 1);
            dtSong = dtSong.AddMonths(dt.Month - 1);
            dtSong = dtSong.AddDays(dt.Day + i - 1);
            return dtSong;
        }

        public void DoShedulerFon(Config conf)
        {
            int i;
            for (i = 0; i < conf.SongFON.Length; i++)
            {
                shedulerFon.Enqueue(conf.SongFON[i]);
            }
        }

        private void CheckCollisions(int minpause)
        {
            NAudio.Wave.AudioFileReader audioFileReadertmp;
            DateTime dt1;
            for (int i = 0; i < ShedulerArray.Count - 1; i++)
            {
                audioFileReadertmp = new NAudio.Wave.AudioFileReader(((Song)shedulerArray[i]).Path);
                // if (Utils.FindDayOfWeek(((Song)ShedulerArray[i])) == DateTime.Now.DayOfWeek)
                // {
                dt1 = ((Song)shedulerArray[i]).FirstTimeStart.AddMinutes(audioFileReadertmp.TotalTime.TotalMinutes).AddMinutes(minpause);
                if (dt1 > ((Song)shedulerArray[i + 1]).FirstTimeStart)
                {
                    while (dt1 > ((Song)shedulerArray[i + 1]).FirstTimeStart)
                    {
                        ((Song)shedulerArray[i + 1]).FirstTimeStart = ((Song)shedulerArray[i + 1]).FirstTimeStart.AddSeconds(1);
                    }
                   // ((Song)shedulerArray[i]).FirstTimeStart = ((Song)shedulerArray[i]).FirstTimeStart.AddMinutes(audioFileReadertmp.TotalTime.TotalMinutes + minpause);
                }
                audioFileReadertmp.Dispose();
                audioFileReadertmp = null;
            }
        }

        public System.Windows.Forms.DataGridViewRow LoadDataGrid()
        {
            return null;
        } 
         

        public ArrayList ShedulerArray { get { return shedulerArray; } set { shedulerArray = value; } }
        internal Queue<Song> ShedulerFon { get {return shedulerFon; } set { shedulerFon = value; } }
    }
}
