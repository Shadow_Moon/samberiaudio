﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamberiAudio
{
    static class Utils
    {

        public static DateTime FindMonday()
        {
            for (int i = 0; i < 8; i++)
            {
                if ((DateTime.Now.AddDays(-i).DayOfWeek) == DayOfWeek.Monday)
                    return DateTime.Now.AddDays(-i);
            }
            return DateTime.Now;
        }

        /// <summary>
        /// Проверяет, должен ли проигрываться трек в дату dt, в день недели j
        /// </summary>
        public static bool FindDayOfWeek(Song song, DateTime dt, int j)
        {
            if (song.Days[0] == '1' && dt.DayOfWeek == DayOfWeek.Monday && j == 0)
                return true;
            if (song.Days[1] == '1' && dt.DayOfWeek == DayOfWeek.Tuesday && j == 1)
                return true;
            if (song.Days[2] == '1' && dt.DayOfWeek == DayOfWeek.Wednesday && j == 2)
                return true;
            if (song.Days[3] == '1' && dt.DayOfWeek == DayOfWeek.Thursday && j == 3)
                return true;
            if (song.Days[4] == '1' && dt.DayOfWeek == DayOfWeek.Friday && j == 4)
            return true;
            if (song.Days[5] == '1' && dt.DayOfWeek == DayOfWeek.Saturday && j == 5)
                return true;
            if (song.Days[6] == '1' && dt.DayOfWeek == DayOfWeek.Sunday && j == 6)
                return true;
            return false;
        }

        /// <summary>
        /// Использовать можно только со списком треков в шедулере
        /// Возврат: Первый день когда будет проигрываться трек.
        /// </summary>
        /// <returns>Первый день когда будет проигрываться трек.</returns>
        public static DayOfWeek FindDayOfWeek(Song song)
        {
            for (int i = 0; i < song.Days.Length; i++)
            {
                if (song.Days[i] == '1')
                    switch (i)
                    {
                        case 0:
                            return DayOfWeek.Monday;
                        case 1:
                            return DayOfWeek.Tuesday;
                        case 2:
                            return DayOfWeek.Wednesday;
                        case 3:
                            return DayOfWeek.Thursday;
                        case 4:
                            return DayOfWeek.Friday;
                        case 5:
                            return DayOfWeek.Saturday;
                        case 6:
                            return DayOfWeek.Sunday;
                        default:
                            break;
                    }
            }
            return DayOfWeek.Monday;
        }

        public static string Date2Time(DateTime dt)
        {
            string str;
            if (dt.Hour <= 9)
            {
                str = "0" + dt.Hour.ToString();
            }
            else
            {
                str = dt.Hour.ToString();
            }
            if (dt.Minute <= 9)
            {
                str += ":" + "0" + dt.Minute.ToString();
            }
            else
            {
                str += ":" + dt.Minute.ToString();
            }
            return str;
        }

               
        private static int DayToInt(DateTime day)
        {
            switch (day.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return 4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                case DayOfWeek.Sunday:
                    return 7;
            }
            return 0;
        }   
    }
}
