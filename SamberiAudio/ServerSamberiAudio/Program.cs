﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocketMessaging;
using ClientTCP;

namespace ServerSamberiAudio
{
    class Program
    {
        private const int PORT = 29501;
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        static SocketMessaging.Server.TcpServer _tcpServer;
        static private System.Collections.ArrayList listAdmins;

        static void Main(string[] args)
        {
            logger.Info("Cтарт");
            listAdmins = new System.Collections.ArrayList();
            _tcpServer = new SocketMessaging.Server.TcpServer();
            _tcpServer.Connected += TcpServer_Connected;
            _tcpServer.Start(PORT);

            Console.WriteLine($"Tcp server started on port {_tcpServer.LocalEndpoint.Port}.");
            Console.WriteLine("Press Enter to stop server.");
            Console.ReadLine();
        }

        private static void TcpServer_Connected(object sender, ConnectionEventArgs e)
        {
            e.Connection.SetMode(MessageMode.DelimiterBound);
            e.Connection.ReceivedMessage += Connection_ReceivedMessage;
            e.Connection.Disconnected += Connection_Disconnected;
            logger.Info($"#{e.Connection.Id} has joined.");


            /*var tcpServer = sender as SocketMessaging.Server.TcpServer;
            var otherClients = tcpServer.Connections.Except(new[] { e.Connection });
            foreach (var client in otherClients)
            {
                logger.Info($"#{client.Id} has joined.");
                logger.Info($"#{e.Connection.Id} has joined.");
                e.Connection.Send($"#{client.Id} has joined.");
                client.Send($"#{e.Connection.Id} has joined.");
            }*/
        }

        private static void Connection_ReceivedMessage(object sender, EventArgs e)
        {
            var connection = sender as Connection;
            var message = connection.ReceiveMessageString();
            logger.Info($"#{connection.Id}: {message}");
            string command = "";
            if (message.Length > 8)
                command = message.Remove(8);
            else
                command = message;
            message = message.Remove(0, 8);
            switch (command)
            {
                case Commands.Alive:
                    //System.Threading.Thread.Sleep(60000);
                    connection.Send(Commands.Alive);
                    if (message != "")
                        SqlClass.Connect(Convert.ToInt32(message));
                    break;
                case Commands.AdmHello:
                    listAdmins.Add(connection.Id);
                    break;
                default:
                    break;
            }
            /*var otherClients = _tcpServer.Connections.Except(new[] { connection });
            foreach (var client in otherClients)
            {
                logger.Info($"#{connection.Id}: {message}");
                client.Send($"#{connection.Id}: {message}");
            }*/
        }

        private static void Connection_Disconnected(object sender, EventArgs e)
        {
            var disconnectedClient = sender as Connection;
            logger.Info($"#{disconnectedClient.Id} has left.");
            listAdmins.Remove(disconnectedClient.Id);
            /*var otherClients = _tcpServer.Connections.Except(new[] { disconnectedClient });
            foreach (var client in otherClients)
            {
                logger.Info($"#{disconnectedClient.Id} has left.");
                client.Send($"#{disconnectedClient.Id} has left.");
            }*/
        }

    }
}
