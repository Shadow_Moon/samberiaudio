﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;


namespace ServerSamberiAudio
{
    static class SqlClass
    {
        static string connectSQL = string.Format("Host={0};Username=SamberiAudio;Password=123456;Database=postgres", ClientTCP.Commands.server);

        public static void Connect(int id)
        {
            using (var conn = new NpgsqlConnection(connectSQL))
            {
                conn.Open();
                // Insert some data
                string read = "";
                using (var cmdread = new NpgsqlCommand(@"SELECT name FROM ""Connected"" WHERE name=:name", conn))
                {
                    cmdread.Parameters.Add(new NpgsqlParameter("name", NpgsqlTypes.NpgsqlDbType.Integer));
                    cmdread.Parameters[0].Value = id;
                    using (var reader = cmdread.ExecuteReader())
                        while(reader.Read())
                            read += reader.GetString(0);
                }
                if (read == "")
                {
                    using (var cmd = new NpgsqlCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = @"INSERT INTO ""Connected"" (name, ""isConnected"") VALUES (:id, :isConnected)";
                        //NpgsqlCommand command = new NpgsqlCommand("update battery set stock = :stock where id = :id;", conn);
                        cmd.Parameters.Add(new NpgsqlParameter("id", NpgsqlTypes.NpgsqlDbType.Integer));
                        cmd.Parameters[0].Value = id;
                        cmd.Parameters.Add(new NpgsqlParameter("isConnected", NpgsqlTypes.NpgsqlDbType.Boolean));
                        cmd.Parameters[1].Value = true;
                        //cmd.Parameters.AddWithValue("p", 6, true);
                        cmd.ExecuteNonQuery();
                    }
                }
                else
                {
                    using (var cmdupdate = new NpgsqlCommand(@"UPDATE ""Connected"" SET ""isConnected"" = TRUE WHERE name=:name", conn))
                    {
                        cmdupdate.Parameters.Add(new NpgsqlParameter("name", NpgsqlTypes.NpgsqlDbType.Integer));
                        cmdupdate.Parameters[0].Value = id;
                        cmdupdate.ExecuteNonQuery();
                    }
                }


            }
        }
    }
}
