﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientTCP
{
    public static class Commands
    {
//        public const string TERMINATOR = "!\r\n\r\n!";
        public const string AdmHello = "AdmHello";
//        public const string CliHello = "CliHello";
        public const string Alive = "Alive   ";

        public static System.Net.IPAddress server = new System.Net.IPAddress(new byte[] { 127, 0, 0, 1 });
        public const int serverport = 29501;
    }
}
