﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PicForRBS
{
    public partial class SelectPic : Form
    {
        string plu = "";
        string selectedPic = "";
        bool ok = false;

        public SelectPic()
        {
            InitializeComponent();
        }
        public SelectPic(string pic)
        {
            InitializeComponent();
            System.IO.StreamReader streamReader = new System.IO.StreamReader(pic);
            Bitmap tmpBitmap = (Bitmap)Bitmap.FromStream(streamReader.BaseStream);
            streamReader.Close();
            pictureBox1.Image = tmpBitmap;

        }

        public string Plu { get => plu; }
        public string SelectedPic { get => selectedPic; }
        public bool Ok { get => ok;  }

        private void button1_Click(object sender, EventArgs e)
        {
            plu = Convert.ToString(maskedTextBox1.Text);
            if (plu.Length != 6)
            {
                System.Windows.Forms.MessageBox.Show("Ввели не 6 цифр Плу");
                return;
            }
            if (pictureBox1.Image == null)
            {
                System.Windows.Forms.MessageBox.Show("Не выбрана картинка");
                return;
            }
            ok = true;
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "png files (*.png)|*.png";
            if (ofd.ShowDialog(this) == DialogResult.OK)
            {
                selectedPic = ofd.FileName;
                if (Image.FromFile(selectedPic).Width != 166 && Image.FromFile(selectedPic).Height != 122)
                {
                    MessageBox.Show("Картинка не 166х122 пикселя");
                    return;
                }
                System.IO.StreamReader streamReader = new System.IO.StreamReader(selectedPic);
                Bitmap tmpBitmap = (Bitmap)Bitmap.FromStream(streamReader.BaseStream);
                streamReader.Close();
                pictureBox1.Image = tmpBitmap;
                label2.Visible = true;
                label2.Text = System.IO.Path.GetFileName(selectedPic);
            }
            
        }
    }
}
