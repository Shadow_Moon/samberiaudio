﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Collections;

namespace PicForRBS
{
    public partial class Form1 : Form
    {
#if DEBUG
        const string xmlpath = "Hierarchy.xml";
#else
        const string xmlpath = @"\\s25-fs01\Общие документы Самбери-25\Hierarchy.xml";
#endif
        Hierarchy hierarchy;
        TreeNode selectedNode = new TreeNode();
        ArrayList pics = new ArrayList();
        ArrayList labels = new ArrayList();

        public Form1()
        {
            InitializeComponent();
            hierarchy = new Hierarchy();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            treeView1.Nodes.Clear();

            using (System.IO.StreamReader sr = new System.IO.StreamReader("Hierarchy.txt"))
            {
                string line;
                line = sr.ReadLine();

                for (int i = 0; line != null; i++)
                {
                    if (Utils.IsDigit(line[0]) == true)
                    {
                        hierarchy.Segment.Add(new Segment(line));
                        treeView1.Nodes.Add(line);
                        while (((line = sr.ReadLine()) != null) && Utils.IsDigit(line[0]) == false)
                        {
                            ((Segment)hierarchy.Segment[i]).Category.Add(new Category(line));
                            treeView1.Nodes[i].Nodes.Add(line);
                        }
                    }
                }
            }
            Loadcfg();
            textBox1.Text = hierarchy.SavePic;

            
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (Utils.IsDigit(treeView1.SelectedNode.Text[0]))
            {
                MessageBox.Show("Выберите категорию, а не сегмент");
            }
            else
            {
                var selectPic = new SelectPic();
                selectPic.ShowDialog(this);
                if (selectPic.Ok == true)
                {
                    Utils.FindHierarchy(hierarchy, treeView1.SelectedNode.Text, selectPic.SelectedPic, selectPic.Plu);
                }
            }
        }

        private void treeView1_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            //treeView1.SelectedNode = e.Node;
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            treeView1.SelectedNode = e.Node;
            if (Utils.IsDigit(e.Node.Text[0]) == false)
            {
                DelPicsAndLabels();

                foreach (Segment segment in hierarchy.Segment)
                {
                    foreach (Category category in segment)
                    {
                        foreach (Goods goods in category)
                        {
                            if (goods.Pic != null && e.Node.Text == category.Name)
                            {
                                pics.Add(new PictureBox());
                                StreamReader streamReader = new StreamReader(goods.Pic);
                                Bitmap tmpBitmap = (Bitmap)Bitmap.FromStream(streamReader.BaseStream);
                                streamReader.Close();
                                int i = 0;
                                ((PictureBox)pics[pics.Count - 1]).Image = tmpBitmap;
                                //((PictureBox)pics[pics.Count - 1]).Image = Image.FromFile(goods.Pic);
                                ((PictureBox)pics[pics.Count - 1]).Width = 166;
                                ((PictureBox)pics[pics.Count - 1]).Height = 122;
                                ((PictureBox)pics[pics.Count - 1]).BorderStyle = BorderStyle.FixedSingle;
                                if (pics.Count % 5 == 0)
                                    ((PictureBox)pics[pics.Count - 1]).Location = new Point(4 * 166, (pics.Count / 5 - 1) * 150);
                                else
                                    ((PictureBox)pics[pics.Count - 1]).Location = new Point((pics.Count % 5 - 1) * 166, pics.Count / 5 * 150);
                                ((PictureBox)pics[pics.Count - 1]).ContextMenuStrip = contextMenuPic;
                                ((PictureBox)pics[pics.Count - 1]).Name = goods.Pic;
                                panel1.Controls.Add(((PictureBox)pics[pics.Count - 1]));

                                labels.Add(new Label());
                                ((Label)labels[labels.Count - 1]).Text = goods.Plu + ".png";
                                if (labels.Count % 5 == 0)
                                    ((Label)labels[labels.Count - 1]).Location = new Point(4 * 166 + 50, (labels.Count / 5  - 1) * 150 + 127);
                                else
                                    ((Label)labels[labels.Count - 1]).Location = new Point((labels.Count % 5 - 1) * 166 + 50, labels.Count / 5 * 150 + 127);
                                panel1.Controls.Add(((Label)labels[labels.Count - 1]));
                            }
                        }
                    }
                }
            }
        }

        private void DelPicsAndLabels()
        {
            for (int i = 0; i < pics.Count - 1; i++)
            {
                ((PictureBox)pics[i]).Dispose();
            }
            pics.Clear();
            for (int i = 0; i < labels.Count - 1; i++)
            {
                ((Label)labels[i]).Dispose();
            }
            labels.Clear();
            panel1.Controls.Clear();
        }

        private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            treeView1.CollapseAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hierarchy.SavePic = textBox1.Text;
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Hierarchy));

            using (var fStream = new FileStream(xmlpath, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                xmlSerializer.Serialize(fStream, hierarchy);
            }
        }

        private void Loadcfg()
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(Hierarchy));
            try
            {
                using (var fStream = new FileStream(xmlpath, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    try
                    {
                        hierarchy = (Hierarchy)xmlSerializer.Deserialize(fStream);
                    }
                    catch (InvalidOperationException)
                    {
                        //logger.Info("Файл config.ini неисправен");
                        return;
                    }
                }
            }
            catch (System.IO.FileNotFoundException)
            {
                //logger.Info("Файла config.ini не найдено");
                return;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {  
            OpenFileDialog folderBrowser = new OpenFileDialog();
            // Set validate names and check file exists to false otherwise windows will
            // not let you select "Folder Selection."
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;            
            // Always default to Folder Selection.
            folderBrowser.FileName = "Выберите папку";
            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = Path.GetDirectoryName(folderBrowser.FileName);
                hierarchy.SavePic = Path.GetDirectoryName(folderBrowser.FileName);
                // ...
            }
        }

        private void добавитьКДругойПЛУToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectPic = new SelectPic(((ContextMenuStrip)((ToolStripItem)sender).Owner).SourceControl.Name);
            selectPic.ShowDialog(this);
            if (selectPic.Ok == true)
            {
                Utils.FindHierarchy(hierarchy, treeView1.SelectedNode.Text, ((ContextMenuStrip)((ToolStripItem)sender).Owner).SourceControl.Name, selectPic.Plu);
            }
        }
    }
}       
