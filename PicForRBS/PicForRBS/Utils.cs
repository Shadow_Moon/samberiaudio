﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PicForRBS
{
    public static class Utils
    {
        public static bool IsDigit(char ch)
        {
            switch (ch)
            {
                case '1':
                    return true;
                case '2':
                    return true;
                case '3':
                    return true;
                case '4':
                    return true;
                case '5':
                    return true;
                case '6':
                    return true;
                case '7':
                    return true;
                case '8':
                    return true;
                case '9':
                    return true;
                case '0':
                    return true;
                default:
                    break;
            }
            return false;
        }

        /// <param name="hierarchy">Структура куда записывать данные</param>
        /// <param name="selectedNode">Выбранная категория для поиска в структуре</param>
        /// <param name="pic">Путь до картинки</param>
        /// <param name="plu">Номер плу</param>
        public static void FindHierarchy(Hierarchy hierarchy, string selectedNode, string pic, string plu)
        {
            /*if (plu == Path.GetFileNameWithoutExtension(pic))
            {
                if (Path.GetDirectoryName(pic) != hierarchy.SavePic)
                {*/
            try
            {
                File.Copy(pic, hierarchy.SavePic + '\\' + plu + ".png", false);
                SaveToCfg(hierarchy, selectedNode, hierarchy.SavePic + '\\' + plu + ".png", plu);
            }
            catch (IOException e)
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Файл {0} существует. Заменить?", hierarchy.SavePic + '\\' + plu + ".png")) == System.Windows.Forms.DialogResult.OK)
                {
                    File.Copy(pic, hierarchy.SavePic + '\\' + plu + ".png", true);
                    SaveToCfg(hierarchy, selectedNode, hierarchy.SavePic + '\\' + plu + ".png", plu);
                }
                //throw;
            }
            /*}
        }
        else
        {
            try
            {
                File.Copy(pic, hierarchy.SavePic + '\\' + plu + ".png", false);
                SaveToCfg(hierarchy, selectedNode, hierarchy.SavePic + '\\' + plu + ".png", plu);
            }
            catch (IOException e)
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Файл {0} существует. Заменить?", hierarchy.SavePic + '\\' + plu + ".png")) == System.Windows.Forms.DialogResult.OK)
                {
                    File.Copy(pic, hierarchy.SavePic + '\\' + plu + ".png", true);
                    SaveToCfg(hierarchy, selectedNode, hierarchy.SavePic + '\\' + plu + ".png", plu);
                }
                //throw;
            }
        }*/
        }

        private static void SaveToCfg(Hierarchy hierarchy, string selectedNode, string pic, string plu)
        {
            for (int i = 0; i < hierarchy.Segment.Count; i++)
            {
                for (int j = 0; j < ((Segment)hierarchy.Segment[i]).Category.Count; j++)
                {
                    if (((Category)((Segment)hierarchy.Segment[i]).Category[j]).Name == selectedNode)
                    {
                        for (int k = 0; k < (((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Count) || (((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Count == 0); k++)
                        {
                            if (((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Count == 0)
                            {
                                ((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Add(new Goods(pic, plu));
                                break;
                            }
                            if (((Goods)((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods[k]).Plu == plu)
                            {
                                var ind = ((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.IndexOf(new Goods(pic, plu));
                                ((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Remove(new Goods(pic, plu));
                                ((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Insert(ind, new Goods(pic, plu));
                                break;
                            }
                            if (((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Count - 1 == k)
                            {
                                ((Category)((Segment)hierarchy.Segment[i]).Category[j]).Goods.Add(new Goods(pic, plu));
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}
