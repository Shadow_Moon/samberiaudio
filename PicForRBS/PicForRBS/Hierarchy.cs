﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Xml.Serialization;

namespace PicForRBS
{
    [XmlInclude(typeof(Segment))]
    [XmlInclude(typeof(Category))]
    [XmlInclude(typeof(Goods))]
    [Serializable]
    public class Hierarchy
    {
        private string savePic;

        public Hierarchy()
        {
            segment = new ArrayList();
        }

        ArrayList segment;

        public ArrayList Segment { get => segment; set => segment = value; }
        public string SavePic { get => savePic; set => savePic = value; }
    }

    
    [Serializable]
    public class Segment
    {
        Segment()
        { }

        public void Add(object value)
        {
            throw new NotSupportedException("Add is not supported for paged results.  Try adding new items to the repository instead.");
        }

        public Segment(string n)
        {
            name = n;
            category = new ArrayList();
        }

        string name;
        ArrayList category;

        public ArrayList Category { get => category; set => category = value; }
        public string Name { get => name; set => name = value; }

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)category).GetEnumerator();
        }
    }

    
    [Serializable]
    public class Category
    {
        Category()
        { }

        public Category(string n)
        {
            name = n;
        }

        ArrayList goods = new ArrayList();

        string name;
        //string pic;
       // string plu;

        public string Name { get => name; set => name = value; }
        public ArrayList Goods { get => goods; set => goods = value; }
        /* public string Pic { get => pic; set => pic = value; }
public string Plu { get => plu; set => plu = value; }*/

        public IEnumerator GetEnumerator()
        {
            return ((IEnumerable)goods).GetEnumerator();
        }
    }

    [Serializable]
    public struct Goods
    {
        public Goods (string _pic, string _plu)
        {
            pic = _pic;
            plu = _plu;
        }
        string pic;
        string plu;

        public string Plu { get => plu; set => plu = value; }
        public string Pic { get => pic; set => pic = value; }
    }
}
